'use strict';
//NODE_ENV=production


const NODE_ENV = process.env.NODE_ENV || 'dev';
const webpack = require ('webpack');
const path = require('path');

var resolveJSON = require('./resolve.json');

function getAlias(json) {
	var result = {};

	for(var name in json) {
		result[name] = path.resolve(__dirname, json[name]);
	}

	return result;
}

module.exports = {

    entry: './src/index',
    output: {
		path : __dirname + '/build',
        filename: "build.js"
        // library: "app_lib"
    },

    watch: NODE_ENV == 'dev', // запускает вебпак как процесс, следит за измненениями и сразу пересобирает проект
    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: NODE_ENV == 'dev' ? "cheep-inline-module-source-map" : null,

    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ],

	resolve: {
		alias : getAlias(resolveJSON)
	}

};

if(NODE_ENV == 'prod') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings :    false,
                drop_console: true,
                unsafe:       true
            }
        })
    )
}
