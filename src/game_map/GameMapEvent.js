/**
 * Created by iShimon on 23.10.16.
 */

function GameMapCellEvent() {}
GameMapCellEvent.GAME_MAP_CELL_CLICK = 'game_map_cell_click';

function GameMapRowEvent() {}
GameMapRowEvent.GAME_MAP_ROW_CLICK = 'game_map_row_click';

function GameMapButtonEvent() {}
GameMapButtonEvent.GAME_MAP_BUTTON_CLICK = 'game_map_button_click';

function GameMapZeroCellEvent() {}
GameMapZeroCellEvent.GAME_MAP_ZERO_CELL_CLICK = 'game_map_zero_cell_click';

function GameMapChangedEvent() {}
GameMapChangedEvent.GAME_MAP_CHANGED = 'game_map_change';

function EllipseGameMapCellEvent() {}
EllipseGameMapCellEvent.ELLIPSE_GAME_MAP_CELL_CLICK = 'ellipse_game_map_cell_click';

function EllipseGameMapSectorEvent() {}
EllipseGameMapSectorEvent.ELLIPSE_GAME_MAP_SECTOR_CLICK = 'ellipse_game_map_sector_click';

function EllipseGameMapChangedEvent() {}
EllipseGameMapChangedEvent.ELLIPSE_GAME_MAP_CHANGED = 'ellipse_game_map_changed';

module.exports = {
	GameMapCellEvent : GameMapCellEvent,
	GameMapRowEvent : GameMapRowEvent,
	GameMapButtonEvent : GameMapButtonEvent,
	GameMapZeroCellEvent : GameMapZeroCellEvent,
	GameMapChangedEvent : GameMapChangedEvent,
	EllipseGameMapCellEvent : EllipseGameMapCellEvent,
	EllipseGameMapSectorEvent : EllipseGameMapSectorEvent,
	EllipseGameMapChangedEvent : EllipseGameMapChangedEvent
};
