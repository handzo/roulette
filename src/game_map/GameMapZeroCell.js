/**
 * Created by iShimon on 24.10.16.
 */

var GameMapChipBox = require('./GameMapChipBox.js');
var GameMapChip = require('./GameMapChip.js');
var GameMapChipType = require('./GameMapEnum.js').GameMapChipType;
var GameMapZeroCellEvent = require('./GameMapEvent.js').GameMapZeroCellEvent;

function GameMapZeroCell(width, height, delta) {

	GameMapZeroCell._super.call(this);

	this.width = width;
	this.height = height;
	this.delta = delta;
	this.chipBoxs = [];

	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.CENTER, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.TOP_SPLIT, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.RIGHT_TOP_SQUARE, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.RIGHT_SPLIT, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.RIGHT_BOTTOM_SQUARE, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.BOTTOM_SPLIT, new GameMapChip()));

	this.activeArea = helper.drawRect(0xFFFFFF, 0, 0, width, height, this);
	this.activeArea.alpha = 0;

	this.hitArea = this.activeArea.getBounds();
	this.interactive = true;

	this.on('mousedown', this.eventZeroCellClick);
	this.on('touchstart', this.eventZeroCellClick);
}

utils.inherits(GameMapZeroCell, PIXI.Graphics);

GameMapZeroCell.prototype.eventZeroCellClick = function(event) {

	var point = event.data.getLocalPosition(this);

	var types = [
		GameMapChipType.TOP_SPLIT,
		GameMapChipType.RIGHT_TOP_SQUARE,
		GameMapChipType.RIGHT_SPLIT,
		GameMapChipType.RIGHT_BOTTOM_SQUARE,
		GameMapChipType.BOTTOM_SPLIT
	];

	var step_y = point.y - this.height / 2;
	var offset = (this.height / 3 - 3 * this.delta);
	var temp = (2 * this.height / 3 - 3 * this.delta);
	var sign = step_y > 0 ? 1 : -1;
	step_y = parseInt(step_y / temp) == 0 ? step_y : sign * (temp - 1);
	var y = parseInt((step_y + sign * offset) / (this.height / 3 - 2 * this.delta)) + 2;

	this.emit(GameMapZeroCellEvent.GAME_MAP_ZERO_CELL_CLICK, {
		type : (point.x < (this.width - this.delta) ? GameMapChipType.CENTER : types[y])
	});
};

GameMapZeroCell.prototype.addChip = function(type, chipBox) {

	var positions = [
		{x : 1.0, 	y : 1 / 6},
		{x : 0.5, 	y : 0.5},
		{x : 1.0, 	y : 5 / 6},
		{x : 1.0, 	y : 1 / 3},
		{x : 1.0, 	y : 0.5},
		{x : 1.0, 	y : 2 / 3}
	];

	var chip = chipBox.getChip();

	type = type - 3;

	if(positions[type] && (!chip.parent)) {
		chip.x = this.x + positions[type].x * this.width;
		chip.y = this.y + positions[type].y * this.height;
		this.parent.addChild(chip);
	}
};

GameMapZeroCell.prototype.animate = function() {
	this.activeArea.removeTweens();
	this.activeArea.alpha = 0.6;
	this.activeArea.addTween(new PIXI.Tween({alpha : 0}, {duration : 1500}));
};

GameMapZeroCell.prototype.addPriceChip = function(type, price) {
	var chipBox = this.getChip(type);
	if(chipBox) {
		this.addChip(type, chipBox);
		chipBox.addPrice(price);
	}
};

GameMapZeroCell.prototype.getPriceChip = function(type) {
	var chipBox = this.getChip(type);
	return chipBox ? chipBox.getPrice() : 0;
};

GameMapZeroCell.prototype.getChip = function(type) {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		if(this.chipBoxs[i].type == type) {
			return this.chipBoxs[i];
		}
	}
	return null;
};

GameMapZeroCell.prototype.removeChip = function(type) {
	var chipBox = this.getChip(type);
	if(chipBox) {
		chipBox.getChip().detach();
		var price = chipBox.getPrice();
		chipBox.setPrice(0);
		return price;
	}
	return 0;
};

GameMapZeroCell.prototype.clearZeroCell = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		this.chipBoxs[i].getChip().detach();
		this.chipBoxs[i].setPrice(0);
	}
};

GameMapZeroCell.prototype.getResult = function() {

	var result = 0.0;
	for(var i = 0; i < this.chipBoxs.length; i++){
		switch (this.chipBoxs[i].getType()) {
			case GameMapChipType.TOP_SPLIT:
			case GameMapChipType.RIGHT_SPLIT:
			case GameMapChipType.BOTTOM_SPLIT: {
				result += this.chipBoxs[i].getPrice() / 2.0;
				break;
			}
			case GameMapChipType.CENTER: {
				result += this.chipBoxs[i].getPrice() / 1.0;
				break;
			}
			case GameMapChipType.RIGHT_TOP_SQUARE:
			case GameMapChipType.RIGHT_BOTTOM_SQUARE: {
				result += this.chipBoxs[i].getPrice() / 3.0;
				break;
			}
		}
	}
	return result;
};

GameMapZeroCell.prototype.action = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		this.chipBoxs[i].getChip().action();
	}
};

GameMapZeroCell.prototype.clear = function() {

	var result = 0;

	for(var i = 0; i < this.chipBoxs.length; i++) {
		result += this.chipBoxs[i].getChip().clear();
	}

	return result;
};

GameMapZeroCell.prototype.getData = function() {

	var data = {
		c : this.getChip(GameMapChipType.CENTER).getPrice(),
		ts : this.getChip(GameMapChipType.TOP_SPLIT).getPrice(),
		rts : this.getChip(GameMapChipType.RIGHT_TOP_SQUARE).getPrice(),
		rs : this.getChip(GameMapChipType.RIGHT_SPLIT).getPrice(),
		rbs : this.getChip(GameMapChipType.RIGHT_BOTTOM_SQUARE).getPrice(),
		bs : this.getChip(GameMapChipType.BOTTOM_SPLIT).getPrice()
	};

	Object.keys(data).forEach(function(key) {
		if(!data[key]) {
			delete data[key];
		}
		else {
			data.s = data.s || 0;
			data.s += data[key];
		}
	});

	return data.s ? data : null;
};

GameMapZeroCell.prototype.setData = function(data) {

	data = data || {};
	this.addPriceChip(GameMapChipType.CENTER, (data.c || 0));
	this.addPriceChip(GameMapChipType.TOP_SPLIT, (data.ts || 0));
	this.addPriceChip(GameMapChipType.RIGHT_TOP_SQUARE, (data.rts || 0));
	this.addPriceChip(GameMapChipType.RIGHT_SPLIT, (data.rs || 0));
	this.addPriceChip(GameMapChipType.RIGHT_BOTTOM_SQUARE, (data.rbs || 0));
	this.addPriceChip(GameMapChipType.BOTTOM_SPLIT, (data.bs || 0));
};

module.exports = GameMapZeroCell;
