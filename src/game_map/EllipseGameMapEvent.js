function EllipseGameMapCellEvent() {}

EllipseGameMapCellEvent.ELLIPSE_GAME_MAP_CELL_CLICK = 'ellipse_game_map_cell_click';

function EllipseGameMapTierEvent() {}
EllipseGameMapTierEvent.ELLIPSE_GAME_MAP_TIER_CLICK = 'ellipse_game_map_tier_click';

function EllipseGameMapOrphelinsEvent() {}
EllipseGameMapOrphelinsEvent.ELLIPSE_GAME_ORPHELINS_CLICK = 'ellipse_game_orphelins_click';

function EllipseGameMapVoisonsEvent() {}
EllipseGameMapVoisonsEvent.ELLIPSE_GAME_VOISONS_CLICK = 'ellipse_game_voisons_click';

function EllipseGameMapZeroSpielEvent() {}
EllipseGameMapZeroSpielEvent.ELLIPSE_GAME_MAP_ZERO_SPIEL_CLICK = 'ellipse_game_map_zero_spiel_click';

function EllipseGameMapEvent() {}
EllipseGameMapEvent.ELLIPSE_GAME_MAP_CHANGE = 'ellipse_game_map_change';

module.exports = {
	EllipseGameMapCellEvent : EllipseGameMapCellEvent,
	EllipseGameMapTierEvent : EllipseGameMapTierEvent,
	EllipseGameMapVoisonsEvent : EllipseGameMapVoisonsEvent,
	EllipseGameMapOrphelinsEvent : EllipseGameMapOrphelinsEvent,
	EllipseGameMapZeroSpielEvent : EllipseGameMapZeroSpielEvent,
	EllipseGameMapEvent : EllipseGameMapEvent
};
