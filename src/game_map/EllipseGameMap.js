var GameMapChipPrice = require('./GameMapEnum').GameMapChipPrice;
var EllipseGameMapCell = require('./EllipseGameMapCell');
var EllipseGameMapChangedEvent = require('./GameMapEvent').EllipseGameMapChangedEvent;
var GameMapSectorType = require('./GameMapEnum').GameMapSectorType;
var GameMapChipType = require('./GameMapEnum').GameMapChipType;
var EllipseGameMapSector = require('./EllipseGameMapSector');
var EllipseGameMapCellEvent = require('./GameMapEvent').EllipseGameMapCellEvent;
var EllipseGameMapEvent = require('./GameMapEvent').EllipseGameMapEvent;

function EllipseGameMap(limit, playerModel) {
    EllipseGameMap._super.call(this);

	this.isBet = true;
	this.maxChips = 5;
	this.rate = 36;
	this.limit = limit;
    this.sectors = [];
	this.playerModel = playerModel;
	this.selectChipPrice = GameMapChipPrice.CHIP_1;

    this.background = helper.createSprite('Roulette', 'EllipseTable', this);

	this.wheel = [32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, // bottom line
				                 30, 8, 23, 10,	// left arc
				                 5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35, // top line
				                 3, 26, 0];		// right arc
	this.xCoords = [1338, 1265, 1192, 1119, 1046, 973, 900, 827, 706, 584, 462, 389, 316, 243,
					90, 135, 180, 225,
					171, 244, 317, 390, 463, 536, 609, 682, 755, 828, 901, 974, 1047, 1120, 1193, 1266,
					270, 330, 390];


	this.innerRad = 92;
	this.outerRad = 168;

    this.dolya = helper.createSprite('Roulette', 'Dolya', null, {}, {x: 0.5, y: 0.5});

	this.width = this.background.width;
	this.height = this.background.height;

	this.map = [];

    var self = this;
	var leftCenter = new PIXI.Point(170, 168);
	var rightCenter = new PIXI.Point(1342, 168);

	var cellHeight = 76,
		cellWidth = 72,
		wideCellWidth = 121,
		angle = 0;

	for (var i = 0; i < this.wheel.length; i++) {
		var center = new PIXI.Point(-1, -1);
		var point1, point2;

		if (i < 14) {
            point1 = new PIXI.Point(this.xCoords[i], 260);
            point2 = new PIXI.Point(point1.x - ((i > 6 && i < 10) ? wideCellWidth : cellWidth) + 1, point1.y + cellHeight);
		}
		else if (i < 18) {
			var angRad1 = helper.degToRad(360 - this.xCoords[i]);
			var angRad2 = helper.degToRad(315 - this.xCoords[i]);
            point1 = new PIXI.Point(leftCenter.x + this.innerRad * Math.cos(angRad1), leftCenter.y - this.innerRad * Math.sin(angRad1));
            point2 = new PIXI.Point(leftCenter.x + this.outerRad * Math.cos(angRad2), leftCenter.y - this.outerRad * Math.sin(angRad2));
            center = leftCenter.clone();
			angle = this.xCoords[i];
		}
		else if (i < 34) {
            point1 = new PIXI.Point(this.xCoords[i], 0);
            point2 = new PIXI.Point(point1.x + cellWidth, point1.y + cellHeight);
		}
		else {
			var angRad1 = helper.degToRad(360 - this.xCoords[i]);
			var angRad2 = helper.degToRad(300 - this.xCoords[i]);
            point1 = new PIXI.Point(rightCenter.x + this.outerRad * Math.cos(angRad1), rightCenter.y - this.outerRad * Math.sin(angRad1));
            point2 = new PIXI.Point(rightCenter.x + this.outerRad * Math.cos(angRad2), rightCenter.y - this.outerRad * Math.sin(angRad2));
            center = rightCenter.clone();
			angle = this.xCoords[i];
		}


		var cell = new EllipseGameMapCell(this.wheel[i], point1, point2, center, this.innerRad, this.outerRad, angle, i);
		cell.on(EllipseGameMapCellEvent.ELLIPSE_GAME_MAP_CELL_CLICK, this.eventEllipseGameMapCellSwitch, this);
		this.map.push(cell);
		this.addChild(cell);
	}

    this.sectorTypes = [
        GameMapSectorType.TIER,
        GameMapSectorType.ORPHELINS,
        GameMapSectorType.VOISONS,
        GameMapSectorType.ZERO_SPIEL
    ];

    var sectorsMap = [[{number : 5, type : GameMapChipType.RIGHT_SPLIT},
		               {number : 10, type : GameMapChipType.TOP_SPLIT},
		               {number : 13, type : GameMapChipType.RIGHT_SPLIT},
		               {number : 23, type : GameMapChipType.TOP_SPLIT},
		               {number : 27, type : GameMapChipType.RIGHT_SPLIT},
		               {number : 33, type : GameMapChipType.RIGHT_SPLIT}],

                      [{number : 1, type : GameMapChipType.CENTER},
			           {number : 6, type : GameMapChipType.RIGHT_SPLIT},
			           {number : 14, type : GameMapChipType.RIGHT_SPLIT},
			           {number : 17, type : GameMapChipType.RIGHT_SPLIT},
			           {number : 31, type : GameMapChipType.RIGHT_SPLIT}],

                      [{number : 3, type : GameMapChipType.LEFT_BOTTOM_SQUARE},
                       {number : 3, type : GameMapChipType.LEFT_BOTTOM_SQUARE},
			           {number : 4, type : GameMapChipType.RIGHT_SPLIT},
			           {number : 12, type : GameMapChipType.RIGHT_SPLIT},
			           {number : 18, type : GameMapChipType.RIGHT_SPLIT},
			           {number : 19, type : GameMapChipType.RIGHT_SPLIT},
			           {number : 25, type : GameMapChipType.RIGHT_TOP_SQUARE},
			           {number : 25, type : GameMapChipType.RIGHT_TOP_SQUARE},
			           {number : 32, type : GameMapChipType.RIGHT_SPLIT}],

                      [{number : 3, type : GameMapChipType.LEFT_SPLIT},
			           {number : 12, type : GameMapChipType.RIGHT_SPLIT},
			           {number : 26, type : GameMapChipType.CENTER},
			           {number : 32, type : GameMapChipType.RIGHT_SPLIT}]];

    var neighbors = [[{number : 8, type : GameMapChipType.LEFT_SPLIT},
                             {number : 11, type : GameMapChipType.BOTTOM_SPLIT},
                             {number : 16, type : GameMapChipType.LEFT_SPLIT},
                             {number : 24, type : GameMapChipType.BOTTOM_SPLIT},
                             {number : 30, type : GameMapChipType.LEFT_SPLIT},
                             {number : 36, type : GameMapChipType.LEFT_SPLIT},
                             {number : 26, type : GameMapChipType.CENTER},
                             {number : 26, type : GameMapChipType.CENTER}],

                            [{number : 9, type : GameMapChipType.LEFT_SPLIT},
                             {number : 17, type : GameMapChipType.LEFT_SPLIT},
                             {number : 20, type : GameMapChipType.LEFT_SPLIT},
                             {number : 34, type : GameMapChipType.LEFT_SPLIT}],

                            [{number : 0, type : GameMapChipType.RIGHT_TOP_SQUARE},
                             {number : 2, type : GameMapChipType.LEFT_TOP_SQUARE},
                             {number : 7, type : GameMapChipType.LEFT_SPLIT},
                             {number : 15, type : GameMapChipType.LEFT_SPLIT},
                             {number : 21, type : GameMapChipType.LEFT_SPLIT},
                             {number : 22, type : GameMapChipType.LEFT_SPLIT},
                             {number : 26, type : GameMapChipType.RIGHT_BOTTOM_SQUARE},
                             {number : 28, type : GameMapChipType.LEFT_TOP_SQUARE},
                             {number : 29, type : GameMapChipType.LEFT_BOTTOM_SQUARE},
                             {number : 35, type : GameMapChipType.LEFT_SPLIT}],

                            [{number : 0, type : GameMapChipType.TOP_SPLIT},
                             {number : 15, type : GameMapChipType.LEFT_SPLIT},
                             {number : 35, type : GameMapChipType.LEFT_SPLIT}]
                           ];

    var points = [
        new PIXI.Point(171, 76),
        new PIXI.Point(464, 76),
        new PIXI.Point(829, 76),
        new PIXI.Point(1286, 76)
    ];

    var widths = [ 290, 362, 512, 55 ];
    var lefts = [ true, false, false, true ];
    var rights = [ false, false, true, true];
    var chipCounts = [ 6, 5, 9, 4 ];
    var cellCounts = [ 12, 8, 17, 7 ];

    for (var i = 0; i < this.sectorTypes.length; i++) {
        var sector = new EllipseGameMapSector(sectorsMap[i], neighbors[i], this.sectorTypes[i], points[i], widths[i], 184, lefts[i], rights[i], chipCounts[i], cellCounts[i]);
        sector.on(EllipseGameMapCellEvent.ELLIPSE_GAME_MAP_SECTOR_CLICK, this.eventEllipseGameMapSectorSwitch, this);
        this.addChild(sector);
        this.sectors.push(sector);
    }
}

utils.inherits(EllipseGameMap, PIXI.Graphics);

EllipseGameMap.prototype.eventEllipseGameMapSectorSwitch = function(event) {
    if (!this.isBet) {
        return;
    }

    var sector;
    for (var i = 0; i < this.sectors.length; i++) {
        if (this.sectors[i].type === event.type) {
            sector = this.sectors[i];
        }
    }

    if (!sector) {
        return;
    }

	if(this.selectChipPrice === GameMapChipPrice.CHIP_DELETE) {
        this.emit(EllipseGameMapChangedEvent.ELLIPSE_GAME_MAP_CHANGED, {mapData : sector.sectorMap});
		var result = sector.removeChip();
		return;
	}

    var maxPriceCell = GameMapChipPrice.CHIP_1 * GameMapChipPrice.maxChips * sector.cellCount;
	var price = sector.getPriceChip();

	if(price >= maxPriceCell) {
		log('AlertLimitBet');
		return;
	}

	var newPrice = ((price + this.selectChipPrice * sector.chipCount) <= maxPriceCell) ? this.selectChipPrice * sector.chipCount : maxPriceCell - price;

	if((this.playerModel.bet + newPrice) > this.limit) {
		newPrice = this.limit - this.playerModel.bet;
        // return; ?
	}

	if(!newPrice) {
		log('AlertLimitTicket');
		return;
	}

	if(this.playerModel.credit < newPrice) {
		newPrice = this.playerModel.credit;
        // return; ?
	}

	if(!newPrice) {
		log('AlertCreditLose');
		return;
	}

	sector.addPriceChip(newPrice);
    sector.animate();

    this.emit(EllipseGameMapChangedEvent.ELLIPSE_GAME_MAP_CHANGED, {mapData : sector.sectorMap});
};

EllipseGameMap.prototype.eventEllipseGameMapCellSwitch = function(event, evevev) {

	if(!this.isBet) {
		return;
	}

	var number = parseInt(event.number);
	var posOnWheel = parseInt(event.posOnWheel);

	var cell = this.map[posOnWheel];
	this.addChip(cell, posOnWheel);

};

EllipseGameMap.prototype.addChip = function(cell, posOnWheel) {

	if (!posOnWheel) {
		posOnWheel = parseInt(cell.getPosOnWheel());
	}

	if(this.selectChipPrice == GameMapChipPrice.CHIP_DELETE)
	{
        this.emit(EllipseGameMapChangedEvent.ELLIPSE_GAME_MAP_CHANGED, {mapData : [{number : cell.getNumber(), type : GameMapChipType.CENTER}]});
		var result = cell.removeChip();
		return;
	}

	var maxPriceCell = GameMapChipPrice.CHIP_1 * GameMapChipPrice.maxChips;
	var price = cell.getPriceChip();

	if(price >= maxPriceCell) {
		log('AlertLimitBet');
		return;
	}

	var newPrice = ((price + this.selectChipPrice) <= maxPriceCell) ? this.selectChipPrice : maxPriceCell - price;

	if((this.playerModel.bet + newPrice) > this.limit) {
		newPrice = this.limit - this.playerModel.bet;
	}

	if(!newPrice) {
		log('AlertLimitTicket');
		return;
	}

	if(this.playerModel.credit < newPrice * this.maxChips) {
        return;
	}

	if(!newPrice) {
		log('AlertCreditLose');
		return;
	}

	var pos = (posOnWheel - 2) < 0 ? 35 + posOnWheel : posOnWheel - 2;
    var mapData = [];

	for (var i = 0; i < 5; i++) {
		this.map[pos].animate();
		this.map[pos].addPriceChip(newPrice);

        mapData.push({number : this.map[pos].getNumber(), type : GameMapChipType.CENTER});

		pos = pos == 36 ? 0 : pos+1;
	}

    if (mapData) {
        this.emit(EllipseGameMapChangedEvent.ELLIPSE_GAME_MAP_CHANGED, {mapData : mapData});
    }

	this.map[posOnWheel].animate();
};

EllipseGameMap.prototype.setSelectChip = function(price) {
	this.selectChipPrice = price;
};

EllipseGameMap.prototype.winNumber = function(number) {

	this.getResult(number);
	this.action();

	if(number) {
		var cell = this.getCell(number);

		this.addChild(this.dolya);
		this.dolya.x = cell.chip.x;
		this.dolya.y = cell.chip.y;
	}
};

EllipseGameMap.prototype.getResult = function(number) {

    var self = this;

    for (var index = 0; index < this.map.length; index++) {
        var cell = this.map[index];

        if (cell.getNumber() == number) {
            cell.getResult();
        }
    }

    for (var index = 0; index < this.sectors.length; index++) {
        var sector = this.sectors[index];
        for (var n = 0; n < sector.sectorMap.length; n++) {
            if (sector.sectorMap[n].number == number) {
                sector.getResult();
            }
        }

        for (var n = 0; n < sector.neighbors.length; n++) {
            if (sector.neighbors[n].number == number) {
                sector.getResult();
            }
        }
    }
};

EllipseGameMap.prototype.clearAll = function() {

	this.clear();
	this.playerModel.credit += this.playerModel.bet;
	this.playerModel.bet = 0;
	this.dolya.detach();
};

EllipseGameMap.prototype.action = function() {

	var result = 0;

    for (var index = 0; index < this.map.length; index++) {
        result += this.map[index].action();
    }

    for (var index = 0; index < this.sectors.length; index++) {
        result += this.sectors[index].action();
    }

	return result;
};

EllipseGameMap.prototype.clear = function() {

	this.dolya.detach();

	var result = 0;

	var elems = [this.map];

	elems.forEach(function(elem) {
		Object.keys(elem).forEach(function(index) {
			result += elem[index].clear();
		});
	});

    for (var index = 0; index < this.sectors.length; index++) {
        result += this.sectors[index].clear();
    }

	return result;
};

EllipseGameMap.prototype.setIsBet = function(state) {
	this.isBet = state;
};

EllipseGameMap.prototype.x2 = function() {

};

EllipseGameMap.prototype.getCell = function(number) {

    for (var index = 0; index < this.map.length; index++) {
        var cell = this.map[index];

        if (cell.getNumber() == number) {
            return cell;
        }
    }
    return null;
};

EllipseGameMap.prototype.getData = function() {

    var result = 0;
	var data = {};

    for (var i = 0; i < this.map.length; i++) {
        result = this.map[i].getData();
        if (result) {
            data['cells'] = data['cells'] || [];
            data['cells'].push(result);
        }
    }

    for (var i = 0; i < this.sectors.length; i++) {
        result = this.sectors[i].getData();
        if (result) {
            data['sectors'] = data['sectors'] || [];
            data['sectors'].push(result);
        }
    }

    return data;
};

EllipseGameMap.prototype.setData = function(data) {

    if (data['cells']) {
        data['cells'].forEach(function(item) {
            var cell = this.map[this.wheel.indexOf(item.n)];
            if (cell && (this.playerModel.credit >= item.c)) {
                cell.setData(item);
            }
        }, this);
    }

    if (data['sectors']) {
        data['sectors'].forEach(function(item) {
            for (var i = 0; i < this.sectors.length; i++) {
                if (this.sectors[i].type === item.t && (this.playerModel.credit >= item.s)) {
                    this.sectors[i].setData(item);
                }
            }
        }, this);
    }
};

EllipseGameMap.prototype.eventGameMapChanged = function(event) {

    var cell = this.getCell(event.number);
    if (cell) {
        cell.removeChip();
    }

    for (var j = 0; j < this.sectors.length; j++) {
        var sector = this.sectors[j];

        if (this.belongToSectorMap(event.number, event.type, sector.sectorMap) ||
            this.belongToSectorMap(event.number, event.type, sector.neighbors)) {
            this.sectors[j].removeChip();
            break;
        }
    }
};

EllipseGameMap.prototype.belongToSectorMap = function (number, type, mapData) {

    for (var i = 0; i < mapData.length; i++) {
        if (mapData[i].number == number && mapData[i].type == type) {
            return true;
        }
    }

    return false;
};

module.exports = EllipseGameMap;

