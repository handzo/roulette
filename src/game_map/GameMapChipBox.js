/**
 * Created by iShimon on 23.10.16.
 */

function GameMapChipBox(type, chip) {

	this.type = type;
	this.chip = chip;
}

GameMapChipBox.prototype.getPrice = function() {
	return this.chip.getPrice();
};

GameMapChipBox.prototype.setPrice = function(price) {
	this.chip.setPrice(price);
};

GameMapChipBox.prototype.addPrice = function(price) {
	this.chip.addPrice(price);
	this.upward();
};

GameMapChipBox.prototype.upward = function() {
 	this.chip.upward();
};

GameMapChipBox.prototype.show = function() {
	this.chip.show();
};

GameMapChipBox.prototype.getChip = function() {
	return this.chip;
};

GameMapChipBox.prototype.setChip = function(chip) {
	this.chip = chip;
};

GameMapChipBox.prototype.getType = function() {
	return this.type;
};

GameMapChipBox.prototype.setIsWin = function(state) {
	if(!this.chip.getIsWin()) {
		this.chip.setIsWin(state);
	}
};

module.exports = GameMapChipBox;