/**
 * Created by iShimon on 24.10.16.
 */

var GameMapButtonEvent = require('./GameMapEvent.js').GameMapButtonEvent;
var GameMapChipBox = require('./GameMapChipBox.js');
var GameMapChip = require('./GameMapChip.js');
var GameMapChipType = require('./GameMapEnum.js').GameMapChipType;

function GameMapButton(number, width, height) {

	GameMapButton._super.call(this);

	this.width = width;
	this.height = height;
	this.number = number;

	this.chipBox = new GameMapChipBox(GameMapChipType.CENTER, new GameMapChip());

	this.activeArea = helper.drawRect(0xFFFFFF, 0, 0, width, height, this);
	this.activeArea.alpha = 0;

	this.hitArea = this.activeArea.getBounds();
	this.interactive = true;

	this.on('mousedown', this.eventButtonClick);
	this.on('touchstart', this.eventButtonClick);
}

utils.inherits(GameMapButton, PIXI.Graphics);

GameMapButton.prototype.eventButtonClick = function(event) {

	this.emit(GameMapButtonEvent.GAME_MAP_BUTTON_CLICK, {
		number : this.number
	});
};

GameMapButton.prototype.getNumber = function() {
	return this.number;
};

GameMapButton.prototype.addPriceChip = function(price) {
	this.addChip();
	this.chipBox.addPrice(price);
};

GameMapButton.prototype.getChip = function() {
	return this.chipBox;
};

GameMapButton.prototype.addChip = function() {

	var chip = this.chipBox.getChip();

	if(!chip.parent) {
		chip.x = this.x + 0.5 * this.width;
		chip.y = this.y + 0.5 * this.height;
		this.parent.addChild(chip);
	}
};

GameMapButton.prototype.getActive = function() {
	return (this.chipBox.getPrice() > 0);
};

GameMapButton.prototype.removeChip = function(type) {

	var chipBox = this.getChip(type);
	if(chipBox) {
		chipBox.getChip().detach();
		var price = chipBox.getPrice();

		chipBox.setPrice(0);
		return price;
	}
	return 0;
};

GameMapButton.prototype.clearButton = function() {

	this.chipBox.setPrice(0);
	this.chipBox.getChip().detach();
};

GameMapButton.prototype.getPriceChip = function() {
	return this.chipBox ? this.chipBox.getPrice() : 0;
};

GameMapButton.prototype.getResult = function() {
	this.chipBox.setIsWin(true);
	return this.chipBox.getPrice();
};

GameMapButton.prototype.action = function() {
	this.chipBox.getChip().action();
};

GameMapButton.prototype.clear = function() {
	return this.chipBox.getChip().clear();
};

GameMapButton.prototype.getData = function() {

	var price = this.getChip(GameMapChipType.CENTER).getPrice();
	return price ? {n : this.number, c : price, s : price} : null;
};

GameMapButton.prototype.setData = function(data) {
	this.addPriceChip(data.c || 0);
};

module.exports = GameMapButton;

