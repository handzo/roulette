/**
 * Created by iShimon on 23.10.16.
 */

var GameMapCell = require('./GameMapCell.js');
var GameMapRow = require('./GameMapRow.js');
var GameMapButton = require('./GameMapButton.js');
var GameMapZeroCell = require('./GameMapZeroCell.js');
var GameMapCellColor = require('./GameMapEnum.js').GameMapCellColor;
var GameMapCellEvent = require('./GameMapEvent.js').GameMapCellEvent;
var GameMapRowEvent = require('./GameMapEvent.js').GameMapRowEvent;
var GameMapButtonEvent = require('./GameMapEvent.js').GameMapButtonEvent;
var GameMapZeroCellEvent = require('./GameMapEvent.js').GameMapZeroCellEvent;
var GameMapChipPrice = require('./GameMapEnum.js').GameMapChipPrice;
var GameMapChipType = require('./GameMapEnum.js').GameMapChipType;
var GameMapChangedEvent = require('./GameMapEvent.js').GameMapChangedEvent;

function GameMap(widthCell, heightCell, lineWidth, deltaCell, limit, playerModel) {

	GameMap._super.call(this);

	this.isBet = true;
	this.rate = 36;
	this.limit = limit;
	this.countRow = 3;
	this.countCol = 12;
	this.countCells = 37;
	this.lineWidth = lineWidth;
	this.deltaCell = deltaCell;
	this.playerModel = playerModel;
	this.widthCell = widthCell + lineWidth;
	this.heightCell = heightCell + lineWidth;
	this.isComplect = false;
	this.selectChipPrice = GameMapChipPrice.CHIP_1;
	this.centerChip = {x : -1, y : -1};
	this.colors = [
		[
			GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.RED, GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.RED,
			GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.RED, GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.RED
		],
		[
			GameMapCellColor.BLACK, GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.BLACK, GameMapCellColor.RED, GameMapCellColor.BLACK,
			GameMapCellColor.BLACK, GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.BLACK, GameMapCellColor.RED, GameMapCellColor.BLACK
		],
		[
			GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.BLACK, GameMapCellColor.RED,
			GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.RED, GameMapCellColor.BLACK, GameMapCellColor.BLACK, GameMapCellColor.RED
		]
	];

	this.background = helper.createSprite('Roulette', 'Table', this, {x: -1 * widthCell - lineWidth, y: -1 * lineWidth});

	this.dolya = helper.createSprite('Roulette', 'Dolya', null, {}, {x: 0.5, y: 0.5});

	this.width = this.background.width;
	this.height = this.background.height;

	this.map = [];
	this.cells = {};
	this.rows = {};
	this.buttons = {};

	for (var i = 0; i < this.countCol; i++) {
		var line = new Array(this.countRow);
		for (var j = (this.countRow - 1); j >= 0; j--) {
			var cell =  new GameMapCell(i * this.countRow + j + 1, this.widthCell, this.heightCell, this.deltaCell, this.colors[this.countRow - j - 1][i]);
			cell.x = i * this.widthCell;
			cell.y = (this.countRow - j - 1) * this.heightCell;
			cell.on(GameMapCellEvent.GAME_MAP_CELL_CLICK, this.eventGameMapCellSwitch, this);
			this.addChild(cell);
			line[j] = cell;

			this.cells[cell.getNumber()] = cell;
		}
		this.map.push(line);
	}

	for (var j = (this.countRow - 1 ); j >= 0; j--) {
		for (var i = 0; i < this.countCol; i++) {
			if (i < (this.countCol - 1)) {
				this.map[i + 1][j].getChip(GameMapChipType.LEFT_TOP_SQUARE).setChip(this.map[i][j].getChip(GameMapChipType.RIGHT_TOP_SQUARE).getChip());
				this.map[i + 1][j].getChip(GameMapChipType.LEFT_SPLIT).setChip(this.map[i][j].getChip(GameMapChipType.RIGHT_SPLIT).getChip());
				this.map[i + 1][j].getChip(GameMapChipType.LEFT_BOTTOM_SQUARE).setChip(this.map[i][j].getChip(GameMapChipType.RIGHT_BOTTOM_SQUARE).getChip());
			}
			if (j > 0) {
				this.map[i][j - 1].getChip(GameMapChipType.LEFT_TOP_SQUARE).setChip(this.map[i][j].getChip(GameMapChipType.LEFT_BOTTOM_SQUARE).getChip());
				this.map[i][j - 1].getChip(GameMapChipType.TOP_SPLIT).setChip(this.map[i][j].getChip(GameMapChipType.BOTTOM_SPLIT).getChip());
				this.map[i][j - 1].getChip(GameMapChipType.RIGHT_TOP_SQUARE).setChip(this.map[i][j].getChip(GameMapChipType.RIGHT_BOTTOM_SQUARE).getChip());
			}
		}
	}

	for(var i = 0; i < this.countCol; i++) {
		var row = new GameMapRow(i + 1, widthCell, widthCell / 2 + lineWidth, deltaCell);
		row.x = this.widthCell * i + lineWidth;
		row.y = this.widthCell * this.countRow;
		row.on(GameMapRowEvent.GAME_MAP_ROW_CLICK, this.eventGameMapRowSwitch, this);
		this.rows[i + 1] = row;
		this.addChild(row);

		if(i > 0) {
			this.rows[i].getChip(GameMapChipType.RIGHT_SPLIT).setChip(row.getChip(GameMapChipType.LEFT_SPLIT).getChip());
		}
	}

	for(var i = 0; i < this.countRow; i++) {
		var button = new GameMapButton(i + 1, widthCell / 2 + lineWidth, heightCell);
		button.x = this.countCol * this.widthCell + lineWidth;
		button.y = this.heightCell * (this.countRow - i - 1);
		button.on(GameMapButtonEvent.GAME_MAP_BUTTON_CLICK, this.eventGameMapButtonSwitch, this);
		this.addChild(button);
		this.buttons[i + 1] = button;
	}

	for(var i = 0; i < 3; i++) {
		var button_width =  (this.widthCell * 12) / 3;
		var button = new GameMapButton(12 * i, button_width, widthCell / 2 + lineWidth);
		button.x = button_width * i +  lineWidth;
		button.y = this.widthCell * this.countRow + widthCell / 2;
		button.on(GameMapButtonEvent.GAME_MAP_BUTTON_CLICK, this.eventGameMapButtonSwitch, this);
		this.addChild(button);
		this.buttons[12 * i] = button;
	}

	for(var i = 0; i < 6; i++) {
		var button_width =  (this.widthCell * 12) / 6;
		var button = new GameMapButton(31 + i, button_width, widthCell / 2 + lineWidth);
		button.x = button_width * i;
		button.y = this.widthCell * this.countRow + 2 * (widthCell / 2 + lineWidth);
		button.on(GameMapButtonEvent.GAME_MAP_BUTTON_CLICK, this.eventGameMapButtonSwitch, this);
		this.addChild(button);
		this.buttons[31 + i] = button;
	}

	this.zeroCell = new GameMapZeroCell(this.widthCell, this.widthCell * 3, this.deltaCell);
	this.zeroCell.x = -1 * this.widthCell;
	this.zeroCell.y = 0;
	this.zeroCell.on(GameMapZeroCellEvent.GAME_MAP_ZERO_CELL_CLICK, this.eventZeroCellClick, this);
	this.addChild(this.zeroCell);

}

utils.inherits(GameMap, PIXI.Graphics);

GameMap.prototype.eventZeroCellClick = function(event) {
	this.centerChip.x = -1;
	this.isComplect = false;
	this.addZeroChip(event.type);
};

GameMap.prototype.addZeroChip = function(type) {

	if(this.selectChipPrice == GameMapChipPrice.CHIP_DELETE) {
		var result = this.zeroCell.removeChip(type);
		this.playerModel.credit += result;
		this.playerModel.bet -= result;
        this.emit(GameMapChangedEvent.GAME_MAP_CHANGED, {number : 0, type : type});
		return;
	}

	var positions = [{
		countCells : 2,
		cells : [[0, 2]]
	}, {
		countCells : 1,
		cells : []
	}, {
		countCells : 2,
		cells : [[0, 0]]
	}, {
		countCells : 3,
		cells : [[0, 1], [0, 2]]
	}, {
		countCells : 2,
		cells : [[0, 1]]
	}, {
		countCells : 3,
		cells : [[0, 0], [0, 1]]
	}];

	if(!positions[parseInt(type - 3)]) {
		return;
	}

	var position = positions[parseInt(type - 3)];

	var maxPriceCell = GameMapChipPrice.CHIP_1 * GameMapChipPrice.maxChips * position.countCells;
	var price = this.zeroCell.getPriceChip(type);

	if(price >= maxPriceCell) {
		log('AlertLimitBet');
		return;
	}

	var newPrice = ((price + this.selectChipPrice) <= maxPriceCell) ? this.selectChipPrice : maxPriceCell - price;

	if((this.playerModel.bet + newPrice) > this.limit) {
		newPrice = this.limit - this.playerModel.bet;
	}

	if(!newPrice) {
		log('AlertLimitTicket');
		return;
	}

	if(this.playerModel.credit < newPrice) {
		newPrice = this.playerModel.credit;
	}

	if(!newPrice) {
		log('AlertCreditLose');
		return;
	}

	this.zeroCell.addPriceChip(type, newPrice);
	this.playerModel.credit -= newPrice;
	this.playerModel.bet += newPrice;

	this.zeroCell.animate();
	for(var i = 0; i < position.cells.length; i++) {
		this.map[position.cells[i][0]][position.cells[i][1]].animate();
	}

	if(type == GameMapChipType.CENTER) {
		this.centerChip.x = -2;
	}
};

GameMap.prototype.eventGameMapButtonSwitch = function(event) {

	if(!this.isBet) {
		return;
	}

	var button = this.getButton(event.number);
	this.centerChip.x = -1;
	this.isComplect = false;

	if(!button) {
		return;
	}

	if(this.selectChipPrice === GameMapChipPrice.CHIP_DELETE) {
		var result = button.removeChip();
		this.playerModel.credit += result;
		this.playerModel.bet -= result;
		return;
	}

	var positions = {
		1 : {countCells : 12},
		2 : {countCells : 12},
		3 : {countCells : 12},
		0 : {countCells : 12},
		12 : {countCells : 12},
		24 : {countCells : 12},
		31 : {countCells : 18},
		32 : {countCells : 18},
		33 : {countCells : 18},
		34 : {countCells : 18},
		35 : {countCells : 18},
		36 : {countCells : 18}
	};

	var position = positions[event.number];

	var maxPriceRow = GameMapChipPrice.CHIP_1 * GameMapChipPrice.maxChips * position.countCells;
	var price = button.getPriceChip();

	if(price >= maxPriceRow) {
		log('AlertLimitBet');
		return;
	}

	var newPrice = ((price + this.selectChipPrice) <= maxPriceRow) ? this.selectChipPrice * position.countCells : maxPriceRow - price;

	if((this.playerModel.bet + newPrice) > this.limit) {
		newPrice = this.limit - this.playerModel.bet;
	}

	if(!newPrice) {
		log('AlertLimitTicket');
		return;
	}

	if(this.playerModel.credit < newPrice) {
		newPrice = this.playerModel.credit;
	}

	if(!newPrice) {
		log('AlertCreditLose');
		return;
	}

	button.addPriceChip(newPrice);
	this.playerModel.credit -= newPrice;
	this.playerModel.bet += newPrice;

	switch(event.number) {
		case 1 :
		case 2 :
		case 3 : {
			for(var i = 0; i < this.countCol; i++) {
				this.map[i][event.number - 1].animate();
			}
			break;
		}
		case 0 :
		case 12 :
		case 24 : {
			for(var i = 0; i < this.countCol; i++) {
				for(var j = 0; j < this.countRow; j++) {
					if((this.map[i][j].getNumber() > event.number) && (this.map[i][j].getNumber() <= (event.number + 12))) {
						this.map[i][j].animate();
					}
				}
			}
			break;
		}
		case 31 :
		case 36 : {
			var step = parseInt((event.number - 31) / 5);
			for(var i = 0; i < this.countCol; i++) {
				for(var j = 0; j < this.countRow; j++) {
					if((this.map[i][j].getNumber() > step * 18) && (this.map[i][j].getNumber() <= ((step + 1) * 18))) {
						this.map[i][j].animate();
					}
				}
			}
			break;
		}
		case 32 :
		case 35 : {
			var step = parseInt((event.number - 32) / 3);
			for(var i = 0; i < this.countCol; i++) {
				for(var j = 0; j < this.countRow; j++) {
					if(this.map[i][j].getNumber() % 2 == step) {
						this.map[i][j].animate();
					}
				}
			}
			break;
		}
		case 33 :
		case 34 : {
			var step = (event.number - 33);
			for(var i = 0; i < this.countCol; i++) {
				for(var j = 0; j < this.countRow; j++) {
					if(this.map[i][j].getColor() == (step + 1)) {
						this.map[i][j].animate();
					}
				}
			}
			break;
		}
	}
};

GameMap.prototype.eventGameMapRowSwitch = function(event) {

	if(!this.isBet) {
		return;
	}

	var x = event.number - 1;
	this.centerChip.x = -1;
	this.isComplect = false;
	var row = this.rows[x + 1];
	this.addRow(event.type, row, x);
};

GameMap.prototype.addRow = function(type, row, x) {

	if(this.selectChipPrice == GameMapChipPrice.CHIP_DELETE) {

		var result = row.removeChip(type);
		this.playerModel.credit += result;
		this.playerModel.bet -= result;
		return;
	}

	var positions = [{
		countCells : this.countRow * 2,
		cells : [0, -1],
		minX : 0
	}, {
		countCells : this.countRow,
		cells : [0]
	}, {
		countCells : this.countRow * 2,
		cells : [0, 1],
		maxX : this.countCol - 1
	}];

	if(!positions[parseInt(type / 3)]) {
		return;
	}

	var position = positions[parseInt(type / 3)];

	if(((position.minX == undefined) || ((position.minX != undefined) && (x > position.minX))) &&
		((position.maxX == undefined) || ((position.maxX != undefined) && (x < position.maxX)))) {

		var maxPriceRow = GameMapChipPrice.CHIP_1 * GameMapChipPrice.maxChips * position.countCells;
		var price = row.getPriceChip(type);

		if(price >= maxPriceRow) {
			log('AlertLimitBet');
			return;
		}

		var newPrice = ((price + this.selectChipPrice) <= maxPriceRow) ? this.selectChipPrice * position.countCells : maxPriceRow - price;

		if((this.playerModel.bet + newPrice) > this.limit) {
			newPrice = this.limit - this.playerModel.bet;
		}

		if(!newPrice) {
			log('AlertLimitTicket');
			return;
		}

		if(this.playerModel.credit < newPrice) {
			newPrice = this.playerModel.credit;
		}

		if(!newPrice) {
			log('AlertCreditLose');
			return;
		}

		row.addPriceChip(type, newPrice);
		this.playerModel.credit -= newPrice;
		this.playerModel.bet += newPrice;

		for(var i = 0; i < position.cells.length; i++) {
			for(var j = 0; j < this.countRow; j++) {
				this.map[x + position.cells[i]][j].animate();
			}
		}
	}
};

GameMap.prototype.eventGameMapCellSwitch = function(event) {

	if(!this.isBet) {
		return;
	}

	var x = parseInt((event.number - 1) / this.countRow);
	var y = parseInt((event.number - 1) - x * this.countRow);

	this.centerChip.x = -1;
	this.isComplect = false;
	var cell = this.map[x][y];
	this.addChip(event.type, cell, x, y);

	if(this.selectChipPrice == GameMapChipPrice.CHIP_DELETE) {
        this.emit(GameMapChangedEvent.GAME_MAP_CHANGED, {number : cell.getNumber(), type : event.type});
    }
};

GameMap.prototype.addChip = function(type, cell, x, y) {

	if(!x && !y) {
		x = parseInt((cell.getNumber() - 1) / this.countRow);
		y = parseInt((cell.getNumber() - 1) - x * this.countRow);
	}

	var zeroCellConf = {
		2 : [[GameMapChipType.LEFT_SPLIT, GameMapChipType.TOP_SPLIT],
			 [GameMapChipType.LEFT_BOTTOM_SQUARE, GameMapChipType.RIGHT_TOP_SQUARE]
		],
		1 : [[GameMapChipType.LEFT_TOP_SQUARE, GameMapChipType.RIGHT_TOP_SQUARE],
			 [GameMapChipType.LEFT_SPLIT, GameMapChipType.RIGHT_SPLIT],
			 [GameMapChipType.LEFT_BOTTOM_SQUARE, GameMapChipType.RIGHT_BOTTOM_SQUARE]
		],
		0 : [[GameMapChipType.LEFT_TOP_SQUARE, GameMapChipType.RIGHT_BOTTOM_SQUARE],
			 [GameMapChipType.LEFT_SPLIT, GameMapChipType.BOTTOM_SPLIT]
		]
	};

	if(x == 0) {
		for(var index in zeroCellConf) {
			if(index == y) {
				for(var i = 0; i < zeroCellConf[index].length; i++) {
					var pair = zeroCellConf[index][i];
					if(pair[0] == type) {
						this.addZeroChip(pair[1]);
						return;
					}
				}
			}
		}
	}

	if(this.selectChipPrice == GameMapChipPrice.CHIP_DELETE)
	{
		var result = cell.removeChip(type);
		this.playerModel.credit += result;
		this.playerModel.bet -= result;
		return;
	}

	var positions = [{
		countCells : 4,
		cells : [[0, 0], [-1, 0], [-1, 1], [0, 1]],
		maxY : this.countRow - 1,
		minX : 0
	}, {
		countCells : 2,
		cells : [[0, 0], [-1, 0]],
		minX : 0
	}, {
		countCells : 4,
		cells : [[0, 0], [-1, 0], [-1, -1], [0, -1]],
		minY : 0,
		minX : 0
	}, {
		countCells : 2,
		cells : [[0, 0], [0, 1]],
		maxY : this.countRow - 1
	}, {
		countCells : 1,
		cells : [[0, 0]]
	}, {
		countCells : 2,
		cells : [[0, 0], [0, -1]],
		minY : 0
	}, {
		countCells : 4,
		cells : [[0, 0], [1, 0], [1, 1], [0, 1]],
		maxX : this.countCol - 1,
		maxY : this.countRow - 1
	}, {
		countCells : 2,
		cells : [[0, 0], [1, 0]],
		maxX : this.countCol - 1
	}, {
		countCells : 4,
		cells : [[0, 0], [1, 0], [1, -1], [0, -1]],
		maxX : this.countCol - 1,
		minY : 0
	}];

	if(!positions[type]) {
		return;
	}

	var position = positions[type];

	if(((position.minX == undefined) || ((position.minX != undefined) && (x > position.minX))) &&
	((position.minY == undefined) || ((position.minY != undefined) && (y > position.minY))) &&
	((position.maxX == undefined) || ((position.maxX != undefined) && (x < position.maxX))) &&
	((position.maxY == undefined) || ((position.maxY != undefined) && (y < position.maxY)))) {

		var maxPriceCell = GameMapChipPrice.CHIP_1 * GameMapChipPrice.maxChips * position.countCells;
		var price = cell.getPriceChip(type);

		if(price >= maxPriceCell) {
			log('AlertLimitBet');
			return;
		}

		var newPrice = ((price + this.selectChipPrice) <= maxPriceCell) ? this.selectChipPrice : maxPriceCell - price;

		if((this.playerModel.bet + newPrice) > this.limit) {
			newPrice = this.limit - this.playerModel.bet;
		}

		if(!newPrice) {
			log('AlertLimitTicket');
			return;
		}

		if(this.playerModel.credit < newPrice) {
			newPrice = this.playerModel.credit;
		}

		if(!newPrice) {
			log('AlertCreditLose');
			return;
		}

		cell.addPriceChip(type, newPrice);
		this.playerModel.credit -= newPrice;
		this.playerModel.bet += newPrice;

		for(var i = 0; i < position.cells.length; i++) {
			this.map[x + position.cells[i][0]][y + position.cells[i][1]].animate();
		}

		if(type == GameMapChipType.CENTER) {
			this.centerChip.x = x;
			this.centerChip.y = y;
		}
	}
};

GameMap.prototype.setSelectChip = function(price) {
	this.selectChipPrice = price;
};

GameMap.prototype.winNumber = function(number) {
	var result = this.getResult(number);
	this.action();

	this.playerModel.bet = 0;
	this.playerModel.credit += result;

	if(number) {
		var cell = this.getCell(number);

		this.addChild(this.dolya);
		this.dolya.x = cell.x + (this.widthCell / 2);
		this.dolya.y = cell.y + (this.heightCell / 2);
	}
};

GameMap.prototype.getResult = function(number) {
	var result = 0.0;

	for(var index in this.cells) {
		var cell = this.cells[index];

		if(cell.getNumber() == number) {
			var color = cell.getColor();
			result += cell.getResult() * this.rate;
		}
	}

	for(var index in this.rows) {
		var row = this.rows[index];

		if((number > (index - 1) * this.countRow) && (number <= ((index - 1) * this.countRow + this.countRow))) {
			result += row.getResult() * this.rate / this.countRow;
		}
	}


	for(var index in this.buttons) {
		var button = this.buttons[index];

		switch (index) {
			case 1:
			case 2:
			case 3: {
				for(var j = 0; j < this.countCol; j++) {
					if(number == this.map[j][index - 1].getNumber()) {
						result += button.getResult() * this.rate / this.countCol;
					}
				}
				break;
			}
			case 31: {
				if((number > 0) && (number <= ((this.countCol * this.countRow) / 2))) {
					result += button.getResult() * this.rate / (this.countRow * this.countCol / 2);
				}
				break;
			}
			case 36: {
				if((number > ((this.countRow * this.countCol) / 2)) && (number <= this.countRow * this.countCol)) {
					result += button.getResult() * this.rate / (this.countRow * this.countCol / 2);
				}
				break;
			}
			case 32: {
				if(number % 2 == 0) {
					result += button.getResult() * this.rate / (this.countRow * this.countCol / 2);
				}
				break;
			}
			case 35: {
				if(number % 2 == 1) {
					result += button.getResult() * this.rate / (this.countRow * this.countCol / 2);
				}
				break;
			}
			case 33: {
				if(color == GameMapCellColor.RED) {
					result += button.getResult() * this.rate / (this.countRow * this.countCol / 2);
				}
				break;
			}
			case 34:{
				if(color == GameMapCellColor.BLACK) {
					result += button.getResult() * this.rate / (this.countRow * this.countCol / 2);
				}
				break;
			}
			default: {
				if ((number > index) && (number <= (index + 12))) {
					result += button.getResult() * this.rate / 12;
				}
				break;
			}
		}
	}

	if(!number) {
		result += this.zeroCell.getResult();
	}

	return result;
};

GameMap.prototype.clearAll = function() {

	this.clear();
	this.playerModel.credit += this.playerModel.bet;
	this.playerModel.bet = 0;
	this.dolya.detach();
};

GameMap.prototype.action = function() {

	var result = 0;

	var elems = [this.cells, this.rows, this.buttons];

	elems.forEach(function(elem) {
		Object.keys(elem).forEach(function(index) {
			result += elem[index].action();
		});
	});

	result += this.zeroCell.action();

	return result;
};

GameMap.prototype.clear = function() {

	this.dolya.detach();

	var result = 0;

	var elems = [this.cells, this.rows, this.buttons];

	elems.forEach(function(elem) {
		Object.keys(elem).forEach(function(index) {
			result += elem[index].clear();
		});
	});

	result += this.zeroCell.clear();

	return result;
};

GameMap.prototype.setIsBet = function(state) {
	this.isBet = state;
};

GameMap.prototype.complect = function() {
	if(this.centerChip.x > -1) {

		if(this.centerChip.y < this.countRow) {

			var chipTypes = [
				GameMapChipType.LEFT_TOP_SQUARE, GameMapChipType.LEFT_SPLIT, GameMapChipType.LEFT_BOTTOM_SQUARE,
				GameMapChipType.TOP_SPLIT, GameMapChipType.BOTTOM_SPLIT, GameMapChipType.RIGHT_TOP_SQUARE,
				GameMapChipType.RIGHT_SPLIT, GameMapChipType.RIGHT_BOTTOM_SQUARE, GameMapChipType.CENTER
			];

			for(var i = 0; i < chipTypes.length; i++) {

				if(chipTypes[i] == GameMapChipType.CENTER) {
					if(!this.isComplect) {
						this.isComplect = true;
						continue;
					}
				}

				this.addChip(chipTypes[i], this.map[this.centerChip.x][this.centerChip.y], this.centerChip.x, this.centerChip.y);
			}
		}
	}
	else if(this.centerChip.x == -2) {
		var chipTypes = [
			GameMapChipType.TOP_SPLIT, GameMapChipType.RIGHT_TOP_SQUARE, GameMapChipType.RIGHT_SPLIT,
			GameMapChipType.RIGHT_BOTTOM_SQUARE, GameMapChipType.BOTTOM_SPLIT, GameMapChipType.CENTER
		];

		for(var i = 0; i < chipTypes.length; i++) {

			if(chipTypes[i] == GameMapChipType.CENTER) {
				if(!this.isComplect) {
					this.isComplect = true;
					continue;
				}
			}

			this.addZeroChip(chipTypes[i]);
		}
	}
};

GameMap.prototype.x2 = function() {

};

GameMap.prototype.getCell = function(number) {
	return this.cells[number] || null;
};

GameMap.prototype.getRow = function(number) {
	return this.rows[number] || null;
};

GameMap.prototype.getButton = function(number) {
	return this.buttons[number] || null;
};

GameMap.prototype.getData = function() {

	var data = {};

	var names = ['cells', 'rows', 'buttons'];

	names.forEach(function(name) {
		Object.keys(this[name]).forEach(function(number) {
			var result = this[name][number].getData();
			if(result) {
				data[name] = data[name] || [];
				data[name].push(result);
			}
		}, this);
	}, this);

	var result =  this.zeroCell.getData();
	if(result) {
		data.zero = result;
	}

	return data;
};

GameMap.prototype.setData = function(data) {

	var names = ['cells', 'rows', 'buttons'];

	names.forEach(function(name) {
		if(data[name]) {
			data[name].forEach(function(item) {
				if(this[name][item.n] && (this.playerModel.credit >= item.s)) {
					this[name][item.n].setData(item);
					this.playerModel.bet += item.s;
					this.playerModel.credit -= item.s;
				}
			}, this);
		}
	}, this);

	if(!data.zero) {
		return;
	}

	this.zeroCell.setData(data.zero);
	this.playerModel.bet += data.zero.s || 0;
	this.playerModel.credit -= data.zero.s || 0;
};

GameMap.prototype.eventEllipseGameMapChanged = function(event) {

	var sectorMap = event.mapData;

	if (!sectorMap) {
		return;
	}

	for (var i = 0; i < sectorMap.length; i++) {
	    var x = parseInt((sectorMap[i].number - 1) / this.countRow);
	    var y = parseInt((sectorMap[i].number - 1) - x * this.countRow);

        if (x < 0 || y < 0) {
            this.addZeroChip(sectorMap[i].type);
        }
        else {
	        var cell = this.map[x][y];
	        this.addChip(sectorMap[i].type, cell, x, y);
        }
	}
    this.centerChip.x = -1;
};

module.exports = GameMap;
