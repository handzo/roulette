var GameMapChip = require('./GameMapChip.js');
var GameMapSectorType = require('./GameMapEnum.js').GameMapSectorType;
var EllipseGameMapCellEvent = require('./GameMapEvent.js').EllipseGameMapCellEvent;

function EllipseGameMapSector(sector, neighbors, type, point, width, height, left, right, chipCount, cellCount) {
    EllipseGameMapSector._super.call(this);

    this.type = type;
    this.width = width;
    this.height = height;
    this.chipCount = chipCount;
    this.cellCount = cellCount;
    this.sectorMap = sector;
    this.neighbors = neighbors;
    this.verticesCount = 5;
    this.radius = height / 2;

    this.chip = new GameMapChip();
    this.chip.x = point.x + width / 2 - (left ? height / 4 : 0) + (right ? height / 4 : 0) - (type == GameMapSectorType.VOISONS ? this.radius: 0);
    this.chip.y = point.y + this.radius;

    this.activeArea = new PIXI.Graphics();
    var hitAreaVertices = [];

    this.activeArea.beginFill(0xFFFFFF, 1);
    this.activeArea.moveTo(point.x, point.y);
    this.activeArea.lineTo(point.x + width, point.y);

    hitAreaVertices.push(new PIXI.Point(point.x, point.y));
    hitAreaVertices.push(new PIXI.Point(point.x + width, point.y));

    if (right) {
        this.activeArea.arc(point.x + width, point.y + this.radius, this.radius, helper.degToRad(270), helper.degToRad(90));
        for (var i = 0; i <= this.verticesCount; i++) {
            var vertAngle = helper.degToRad(90 + i * 180 / this.verticesCount);
            hitAreaVertices.push(new PIXI.Point(point.x + width - this.radius * Math.cos(vertAngle),
                                                point.y + this.radius - this.radius * Math.sin(vertAngle)));
        }
    }
    else {
        this.activeArea.lineTo(point.x + width, point.y + height);
        hitAreaVertices.push(new PIXI.Point(point.x + width, point.y + height));
    }

    this.activeArea.lineTo(point.x, point.y + height);
    hitAreaVertices.push(new PIXI.Point(point.x, point.y + height));

    if (left) {
        this.activeArea.arc(point.x, point.y + this.radius, this.radius, helper.degToRad(90), helper.degToRad(270));
        for (var i = this.verticesCount; i >= 0; i--) {
            var vertAngle = helper.degToRad(270 - i * 180 / this.verticesCount);
            hitAreaVertices.push(new PIXI.Point(point.x + this.radius * Math.cos(vertAngle),
                                                point.y + this.radius + this.radius * Math.sin(vertAngle)));
        }
    }
    else {
        hitAreaVertices.push(new PIXI.Point(point.x, point.y));
        this.activeArea.lineTo(point.x, point.y);
    }

    this.activeArea.endFill();

    this.activeArea.alpha = 0;
    this.addChild(this.activeArea);

    this.hitArea = new PIXI.Polygon(hitAreaVertices);
    this.interactive = true;

	this.on('mousedown', this.eventTierClick);
	this.on('touchstart', this.eventTierClick);
}

utils.inherits(EllipseGameMapSector, PIXI.Graphics);

EllipseGameMapSector.prototype.eventTierClick = function(event) {
	this.emit(EllipseGameMapCellEvent.ELLIPSE_GAME_MAP_SECTOR_CLICK, {type : this.type});
};

EllipseGameMapSector.prototype.animate = function() {
    this.activeArea.removeTweens();
    this.activeArea.alpha = 0.6;
    this.activeArea.addTween(new PIXI.Tween({alpha : 0}, {duration : 1500}));
};

EllipseGameMapSector.prototype.action = function() {
    this.chip.action();
};

EllipseGameMapSector.prototype.addPriceChip = function(price) {
    if (this.chip) {
        this.chip.addPrice(price);
        this.addChip();
    }
};

EllipseGameMapSector.prototype.addChip = function() {
    this.chip.reskin();
    this.parent.addChild(this.chip);
};

EllipseGameMapSector.prototype.getChip = function() {
    return this.chip ? this.chip : null;
};

EllipseGameMapSector.prototype.removeChip = function() {

    if (this.chip) {
        var result = this.chip.getPrice();
        this.chip.setPrice(0);
        this.chip.detach();
        return result;
    }

    return 0;
};

EllipseGameMapSector.prototype.getData = function() {

    var data = {
        t : this.type,
        s : this.chip.getPrice()
    };

    return data.s ? data : null;
};

EllipseGameMapSector.prototype.setData = function(data) {

    this.addPriceChip(data.s);
};

EllipseGameMapSector.prototype.clear = function() {
    this.removeChip();
};

EllipseGameMapSector.prototype.getPriceChip = function() {
    return this.chip ? this.chip.getPrice() : 0;
};

EllipseGameMapSector.prototype.getResult = function() {

    this.chip.setIsWin(true);
    return this.getPriceChip();
};

module.exports = EllipseGameMapSector;
