var GameMapChip = require('./GameMapChip.js');
var EllipseGameMapCellEvent = require('./EllipseGameMapEvent.js').EllipseGameMapCellEvent;

function EllipseGameMapCell(number, point1, point2, center, innerRad, outerRad, angle, posOnWheel) {

	EllipseGameMapCell._super.call(this);

	this.number = number;
	this.posOnWheel = posOnWheel;
	this.innerRad = innerRad;
	this.outerRad = outerRad;
	this.verticesCount = 6;

	this.chip = new GameMapChip();

	this.activeArea = new PIXI.Graphics();
	this.activeArea.beginFill(0xFFFFFF, 1);

	if (center.x == -1 && center.y == -1) {
		this.activeArea.drawRect(point1.x, point1.y, point2.x - point1.x, point2.y - point1.y);
		this.chip.x = (point1.x + point2.x) / 2;
		this.chip.y = (point1.y + point2.y) / 2;

		this.hitArea = this.activeArea.getBounds();
	}
	else {
		var dangle = (number == 0 || number == 3 || number == 26) ? 60 : 45;
		this.activeArea.moveTo(point1.x, point1.y);
		this.activeArea.arc(center.x, center.y, innerRad, angle * Math.PI / 180, (angle + dangle) * Math.PI / 180, false);
		this.activeArea.lineTo(point2.x, point2.y);
		this.activeArea.arc(center.x, center.y, outerRad, (angle + dangle) * Math.PI / 180, angle * Math.PI / 180, true);
		this.activeArea.lineTo(point1.x, point1.y);

		var hitAreaVertices = [];

		hitAreaVertices.push(new PIXI.Point(point1.x, point1.y));
		for (var i = 0; i <= this.verticesCount; i++) {
			var vertAngle = helper.degToRad(angle + i * dangle / this.verticesCount);
			hitAreaVertices.push(new PIXI.Point(center.x + this.innerRad * Math.cos(vertAngle), center.y + this.innerRad * Math.sin(vertAngle)));
		}

		hitAreaVertices.push(new PIXI.Point(point2.x, point2.y));

		for (var i = this.verticesCount; i >= 0; i--) {
			var vertAngle = helper.degToRad(angle + i * dangle / this.verticesCount);
			hitAreaVertices.push(new PIXI.Point(center.x + this.outerRad * Math.cos(vertAngle), center.y + this.outerRad * Math.sin(vertAngle)));
		}

		this.hitArea = new PIXI.Polygon(hitAreaVertices);

		this.chip.x = center.x + (innerRad + outerRad) / 2 * Math.cos((2 * angle + dangle) * Math.PI / 360);
		this.chip.y = center.y + (innerRad + outerRad) / 2 * Math.sin((2 * angle + dangle) * Math.PI / 360);
	}

	this.activeArea.endFill();
	this.activeArea.alpha = 0;
	this.addChild(this.activeArea);

	this.interactive = true;

	this.on('mousedown', this.eventCellClick);
	this.on('touchstart', this.eventCellClick);
}

utils.inherits(EllipseGameMapCell, PIXI.Graphics);

EllipseGameMapCell.prototype.eventCellClick = function(event) {

	this.emit(EllipseGameMapCellEvent.ELLIPSE_GAME_MAP_CELL_CLICK, {
		posOnWheel : this.posOnWheel,
		number : this.number
	});
};

EllipseGameMapCell.prototype.animate = function() {
	this.activeArea.removeTweens();
	this.activeArea.alpha = 0.6;
	this.activeArea.addTween(new PIXI.Tween({alpha : 0}, {duration : 1500}));
};

EllipseGameMapCell.prototype.getNumber = function() {
	return this.number;
};

EllipseGameMapCell.prototype.getPosOnWheel = function() {
	return this.posOnWheel;
};

EllipseGameMapCell.prototype.getChip = function() {

	return this.chip || null;
};

EllipseGameMapCell.prototype.addPriceChip = function(price) {

	if (this.chip) {
		this.chip.addPrice(price);
		this.addChip();
	}
};

EllipseGameMapCell.prototype.addChip = function() {

	this.chip.reskin();
	this.parent.addChild(this.chip);
};

EllipseGameMapCell.prototype.removeChip = function() {

	this.chip.detach();
	var price = this.chip.getPrice();
	this.chip.setPrice(0);
	return price;
};

EllipseGameMapCell.prototype.getPriceChip = function() {
	return this.chip.getPrice();
};

EllipseGameMapCell.prototype.show = function() {
	log("Chip Price", this.chip.getPrice());
};

EllipseGameMapCell.prototype.clearCell = function() {

	this.chip.detach();
	this.chip.setPrice(0);
};

EllipseGameMapCell.prototype.getActive = function() {

	return this.chip.getPrice() > 0 ? true : false;
};

EllipseGameMapCell.prototype.getResult = function(){

	this.chip.setIsWin(true);
	return this.chip.getPrice();
};

EllipseGameMapCell.prototype.action = function() {
	this.chip.action();
};

EllipseGameMapCell.prototype.clear = function() {

	return this.chip.clear();
};

EllipseGameMapCell.prototype.getData = function() {

	var data = {
		n : this.number,
		c : this.chip.getPrice()
	};

	return data.c ? data : null;
};

EllipseGameMapCell.prototype.setData = function(data) {

	this.addPriceChip(data.c);
};

module.exports = EllipseGameMapCell;
