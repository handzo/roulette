/**
 * Created by iShimon on 23.10.16.
 */

function GameMapChipType() {}

GameMapChipType.LEFT_TOP_SQUARE = 0;
GameMapChipType.LEFT_SPLIT = 1;
GameMapChipType.LEFT_BOTTOM_SQUARE = 2;
GameMapChipType.TOP_SPLIT = 3;
GameMapChipType.CENTER = 4;
GameMapChipType.BOTTOM_SPLIT = 5;
GameMapChipType.RIGHT_TOP_SQUARE = 6;
GameMapChipType.RIGHT_SPLIT = 7;
GameMapChipType.RIGHT_BOTTOM_SQUARE = 8;
GameMapChipType.NONE = 9;

function GameMapCellColor() {}

GameMapCellColor.RED = 1;
GameMapCellColor.BLACK = 2;

function GameMapChipPrice() {}

GameMapChipPrice.CHIP_1 = 10;
GameMapChipPrice.CHIP_2 = 25;
GameMapChipPrice.CHIP_3 = 50;
GameMapChipPrice.CHIP_4 = 100;
GameMapChipPrice.CHIP_5 = 500;
GameMapChipPrice.CHIP_DELETE = -1;
GameMapChipPrice.CHIP_NONE = 0;
GameMapChipPrice.maxChips = 20;

function GameMapSectorType() {}

GameMapSectorType.TIER = 0;
GameMapSectorType.ORPHELINS = 1;
GameMapSectorType.VOISONS = 2;
GameMapSectorType.ZERO_SPIEL = 3;

module.exports = {
	GameMapChipType : GameMapChipType,
	GameMapCellColor : GameMapCellColor,
	GameMapChipPrice : GameMapChipPrice,
	GameMapSectorType : GameMapSectorType
};
