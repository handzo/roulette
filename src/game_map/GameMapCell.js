/**
 * Created by iShimon on 23.10.16.
 */

var GameMapChipType = require('./GameMapEnum.js').GameMapChipType;
var GameMapCellColor = require('./GameMapEnum.js').GameMapCellColor;
var GameMapChip = require('./GameMapChip.js');
var GameMapChipBox = require('./GameMapChipBox.js');
var GameMapCellEvent = require('./GameMapEvent.js').GameMapCellEvent;

function GameMapCell(number, width, height, delta, color) {

	GameMapCell._super.call(this);

	this.width = width;
	this.height = height;
	this.number = number;
	this.delta = delta;
	this.color = color;
	this.chipBoxs = [];

	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.LEFT_TOP_SQUARE, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.LEFT_SPLIT, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.LEFT_BOTTOM_SQUARE, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.TOP_SPLIT, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.CENTER, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.BOTTOM_SPLIT, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.RIGHT_TOP_SQUARE, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.RIGHT_SPLIT, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.RIGHT_BOTTOM_SQUARE, new GameMapChip()));

	this.activeArea = helper.drawRect(0xFFFFFF, 0, 0, width, height, this);
	this.activeArea.alpha = 0;

	this.hitArea = this.activeArea.getBounds();
	this.interactive = true;

	this.on('mousedown', this.eventCellClick);
	this.on('touchstart', this.eventCellClick);
}

utils.inherits(GameMapCell, PIXI.Graphics);

GameMapCell.prototype.eventCellClick = function(event) {

	var point = event.data.getLocalPosition(this);

	var types = [
		GameMapChipType.LEFT_TOP_SQUARE,
		GameMapChipType.TOP_SPLIT,
		GameMapChipType.RIGHT_TOP_SQUARE,
		GameMapChipType.LEFT_SPLIT,
		GameMapChipType.CENTER,
		GameMapChipType.RIGHT_SPLIT,
		GameMapChipType.LEFT_BOTTOM_SQUARE,
		GameMapChipType.BOTTOM_SPLIT,
		GameMapChipType.RIGHT_BOTTOM_SQUARE
	];

	var offset = (this.width - 3 * this.delta);
	var center_width = (this.width - 2 * this.delta);

	var x = parseInt((point.x + offset) / center_width);
	var y = parseInt((point.y + offset) / center_width);

	this.emit(GameMapCellEvent.GAME_MAP_CELL_CLICK, {
		type : (types[x + y * 3] != undefined ? types[x + y * 3] : GameMapChipType.NONE),
		number : this.number
	});
};

GameMapCell.prototype.animate = function() {
	this.activeArea.removeTweens();
	this.activeArea.alpha = 0.6;
	this.activeArea.addTween(new PIXI.Tween({alpha : 0}, {duration : 1500}));
};

GameMapCell.prototype.getNumber = function() {
	return this.number;
};

GameMapCell.prototype.getColor = function() {
	return this.color;
};

GameMapCell.prototype.getChip = function(type) {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		if(this.chipBoxs[i].type == type) {
			return this.chipBoxs[i];
		}
	}
	return null;
};

GameMapCell.prototype.addPriceChip = function(type, price) {
	var chipBox = this.getChip(type);
	if(chipBox) {
		this.addChip(type, chipBox);
		chipBox.addPrice(price);
	}
};

GameMapCell.prototype.addChip = function(type, chipBox) {

	var positions = [
		{x : 0, 	y : 0},
		{x : 0, 	y : 0.5},
		{x : 0, 	y : 1},
		{x : 0.5, 	y : 0},
		{x : 0.5, 	y : 0.5},
		{x : 0.5, 	y : 1},
		{x : 1,		y : 0},
		{x : 1, 	y : 0.5},
		{x : 1, 	y : 1}
	];

	var chip = chipBox.getChip();

	if(positions[type] && (!chip.parent)) {
		chip.x = this.x + positions[type].x * this.width;
		chip.y = this.y + positions[type].y * this.height;
		this.parent.addChild(chip);
	}
};

GameMapCell.prototype.removeChip = function(type) {

	var chipBox = this.getChip(type);
	if(chipBox) {
		chipBox.getChip().detach();
		var price = chipBox.getPrice();
		chipBox.setPrice(0);
		return price;
	}
	return 0;
};

GameMapCell.prototype.getPriceChip = function(type) {
	var chipBox = this.getChip(type);
	return chipBox ? chipBox.getPrice() : 0;
};

GameMapCell.prototype.show = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		log("Type, Price", this.chipBoxs[i].type, this.chipBoxs[i].getPrice());
	}
};

GameMapCell.prototype.clearCell = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		//chipBoxs[i]->getChip()->setPrice(0);
		this.chipBoxs[i].getChip().detach();
		this.chipBoxs[i].setPrice(0);
	}
};

GameMapCell.prototype.getActive = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		if(this.chipBoxs[i].getPrice() > 0) {
			return true;
		}
	}
	return false;
};

GameMapCell.prototype.getResult = function(){

	var result = 0.0;
	for(var i = 0; i < this.chipBoxs.length; i++) {
		switch (this.chipBoxs[i].getType()) {
			case GameMapChipType.LEFT_TOP_SQUARE:
			case GameMapChipType.LEFT_BOTTOM_SQUARE:
			case GameMapChipType.RIGHT_TOP_SQUARE:
			case GameMapChipType.RIGHT_BOTTOM_SQUARE:{
				result += this.chipBoxs[i].getPrice() / 4.0;
				break;
			}
			case GameMapChipType.LEFT_SPLIT:
			case GameMapChipType.TOP_SPLIT:
			case GameMapChipType.RIGHT_SPLIT:
			case GameMapChipType.BOTTOM_SPLIT:{
				result += this.chipBoxs[i].getPrice() / 2.0;
				break;
			}
			case GameMapChipType.CENTER:{
				result += this.chipBoxs[i].getPrice() / 1.0;
				break;
			}
		}

		this.chipBoxs[i].setIsWin(true);

	}

	return result;
};

GameMapCell.prototype.action = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		this.chipBoxs[i].getChip().action();
	}
};

GameMapCell.prototype.clear = function() {

	var result = 0;

	for(var i = 0; i < this.chipBoxs.length; i++) {
		result += this.chipBoxs[i].getChip().clear();
	}

	return result;
};

GameMapCell.prototype.getData = function() {

	var data = {
		n : this.number,
		c : this.getChip(GameMapChipType.CENTER).getPrice(),
		ts : this.getChip(GameMapChipType.TOP_SPLIT).getPrice(),
		rs : this.getChip(GameMapChipType.RIGHT_SPLIT).getPrice(),
		rts : this.getChip(GameMapChipType.RIGHT_TOP_SQUARE).getPrice()
	};

	Object.keys(data).forEach(function(key) {
		if(!data[key]) {
			delete data[key];
		}
		else if(key != 'n') {
			data.s = data.s || 0;
			data.s += data[key];
		}
	});

	return data.s ? data : null;
};

GameMapCell.prototype.setData = function(data) {

	this.addPriceChip(GameMapChipType.CENTER, (data.c || 0));
	this.addPriceChip(GameMapChipType.TOP_SPLIT, (data.ts || 0));
	this.addPriceChip(GameMapChipType.RIGHT_SPLIT, (data.rs || 0));
	this.addPriceChip(GameMapChipType.RIGHT_TOP_SQUARE, (data.rts || 0));
};

module.exports = GameMapCell;
