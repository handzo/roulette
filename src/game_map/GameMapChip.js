/**
 * Created by iShimon on 23.10.16.
 */

var GameMapChipPrice = require('./GameMapEnum.js').GameMapChipPrice;

function GameMapChip() {

	GameMapChip._super.call(this);

	this.price = 0;
	this.isWin = false;

	this.winSkin = new PIXI.Sprite();
	this.winSkin.anchor.x = 0.5;
	this.winSkin.anchor.y = 0.5;

	this.skin = new PIXI.Sprite();
	this.skin.anchor.x = 0.5;
	this.skin.anchor.y = 0.5;
	this.addChild(this.skin);

	this.style = {
		fontFamily : 'Arial',
		fontSize : '19px',
		fill : '#F7EDCA',
	};

	this.text = helper.createText('', this.style, this.skin, {}, {x: 0.5, y: 0.5});
}

utils.inherits(GameMapChip, PIXI.Sprite);

GameMapChip.prototype.getPrice = function() {
	return this.price;
};

GameMapChip.prototype.show = function() {
	log('Chip price', this.price);
};

GameMapChip.prototype.addPrice = function(price) {
//	config::mPlayer.soundPlay("Chip");

	this.setPrice(this.price + price);
};

GameMapChip.prototype.setPrice = function(price) {

	if(!price) {
		this.detach();
	}

	this.price = price;
	this.reskin();
};

GameMapChip.prototype.clear = function() {

	this.isWin = false;
	this.skin.removeTweens();
	this.skin.scale = {x : 1.0, y : 1.0};

	this.winSkin.removeTweens();
	this.winSkin.detach();

	var result = this.price;
	this.setPrice(0);
	return result;
};

GameMapChip.prototype.reskin = function() {

	this.text.text = (this.price ? this.price : "");

	if(this.price < GameMapChipPrice.CHIP_1) {
		this.skin.detach();
		return;
	}

	var prices = [GameMapChipPrice.CHIP_1, GameMapChipPrice.CHIP_2, GameMapChipPrice.CHIP_3, GameMapChipPrice.CHIP_4, GameMapChipPrice.CHIP_5];
	var styles = [helper.rgbToHex(250, 107, 0), helper.rgbToHex(25, 113, 12), helper.rgbToHex(209, 0, 129), helper.rgbToHex(57, 136, 245), helper.rgbToHex(80, 51, 42)];

	for (var i = 0; i < prices.length; i++) {
		if((i == (prices.length - 1)) || ((this.price >= prices[i]) && (this.price < prices[i + 1]))) {
			this.skin.setResAnim(helper.createTexture('Roulette', 'FieldChip' + (i + 1)));
			this.style.fill = styles[i];
			break;
		}
	}

	if(!this.skin.parent) {
		this.addChild(this.skin);
	}

	this.text.style = this.style;
};

GameMapChip.prototype.setIsWin = function(state) {
	this.isWin = state;
};

GameMapChip.prototype.getIsWin = function() {
	return this.isWin;
};

GameMapChip.prototype.action = function (speed, res) {

	speed = speed || 1500;

	if(this.price < GameMapChipPrice.CHIP_1) {
		return;
	}

	if(!this.isWin) {
		this.skin.removeTweens();
		this.skin.scale = {x : 1.0, y : 1.0};

		if(!this.skin.parent) {
			this.addChild(this.skin);
		}

		var tweenScale = new PIXI.Tween({scale : {x : 0.0, y : 0.0}}, {duration : 3000});

		tweenScale.on(PIXI.TweenEvent.COMPLETE, function() {
			this.skin.detach();
		}, this);

		this.skin.addTween(tweenScale);
	}
};

module.exports = GameMapChip;
