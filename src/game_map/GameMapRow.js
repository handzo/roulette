/**
 * Created by iShimon on 24.10.16.
 */

var GameMapRowEvent = require('./GameMapEvent.js').GameMapRowEvent;
var GameMapChipType = require('./GameMapEnum.js').GameMapChipType;
var GameMapChipBox = require('./GameMapChipBox.js');
var GameMapChip = require('./GameMapChip.js');

function GameMapRow(number, width, height, delta) {

	GameMapRow._super.call(this);

	this.width = width;
	this.height = height;
	this.number = number;
	this.delta = delta;
	this.chipBoxs = [];

	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.LEFT_SPLIT, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.CENTER, new GameMapChip()));
	this.chipBoxs.push(new GameMapChipBox(GameMapChipType.RIGHT_SPLIT, new GameMapChip()));

	this.activeArea = helper.drawRect(0xFFFFFF, 0, 0, width, height, this);
	this.activeArea.alpha = 0;

	this.hitArea = this.activeArea.getBounds();
	this.interactive = true;

	this.on('mousedown', this.eventRowClick);
	this.on('touchstart', this.eventRowClick);
}

utils.inherits(GameMapRow, PIXI.Graphics);

GameMapRow.prototype.eventRowClick = function(event) {

	var point = event.data.getLocalPosition(this);

	var types = [
		GameMapChipType.LEFT_SPLIT,
		GameMapChipType.CENTER,
		GameMapChipType.RIGHT_SPLIT
	];

	var offset = (this.width - 3 * this.delta);
	var center_width = (this.width - 2 * this.delta);

	var x = parseInt((point.x + offset) / center_width);

	this.emit(GameMapRowEvent.GAME_MAP_ROW_CLICK, {
		type : (types[x] != undefined ? types[x] : GameMapChipType.NONE),
		number : this.number
	});
};


GameMapRow.prototype.getNumber = function() {
	return this.number;
};

GameMapRow.prototype.addPriceChip = function(type, price) {
	var chipBox = this.getChip(type);
	if(chipBox) {
		this.addChip(type, chipBox);
		chipBox.addPrice(price);
	}
};

GameMapRow.prototype.addChip = function(type, chipBox) {

	var positions = [
		{x : 0, 	y : 0.5},
		{x : 0.5, 	y : 0.5},
		{x : 1, 	y : 0.5}
	];

	var chip = chipBox.getChip();
	type = parseInt(type / 3);

	if(positions[type] && (!chip.parent)) {
		chip.x = this.x + positions[type].x * this.width;
		chip.y = this.y + positions[type].y * this.height;
		this.parent.addChild(chip);
	}
};

GameMapRow.prototype.getChip = function(type) {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		if(this.chipBoxs[i].type == type) {
			return this.chipBoxs[i];
		}
	}
	return null;
};

GameMapRow.prototype.removeChip = function(type) {

	var chipBox = this.getChip(type);
	if(chipBox) {
		chipBox.getChip().detach();
		var price = chipBox.getPrice();
		chipBox.setPrice(0);
		return price;
	}
	return 0;
};

GameMapRow.prototype.getActive = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		if(this.chipBoxs[i].getPrice() > 0)
		{
			return true;
		}
	}
	return false;
};

GameMapRow.prototype.clearRow = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		this.chipBoxs[i].getChip().detach();
		this.chipBoxs[i].setPrice(0);
	}
};

GameMapRow.prototype.getPriceChip = function(type) {
	var chipBox = this.getChip(type);
	return chipBox ? chipBox.getPrice() : 0;
};

GameMapRow.prototype.getResult = function() {

	var result = 0.0;
	for(var i = 0; i < this.chipBoxs.length; i++) {
		switch (this.chipBoxs[i].getType()) {
			case GameMapChipType.LEFT_SPLIT:
			case GameMapChipType.RIGHT_SPLIT: {
				result += this.chipBoxs[i].getPrice() / 2.0;
				break;
			}
			case GameMapChipType.CENTER: {
				result += this.chipBoxs[i].getPrice() / 1.0;
				break;
			}
		}

		this.chipBoxs[i].setIsWin(true);
	}
	return result;
};

GameMapRow.prototype.action = function() {
	for(var i = 0; i < this.chipBoxs.length; i++) {
		this.chipBoxs[i].getChip().action();
	}
};

GameMapRow.prototype.clear = function() {

	var result = 0;

	for(var i = 0; i < this.chipBoxs.length; i++) {
		result += this.chipBoxs[i].getChip().clear();
	}

	return result;
};

GameMapRow.prototype.getData = function() {

	var data = {
		n : this.number,
		c : this.getChip(GameMapChipType.CENTER).getPrice(),
		rs : this.getChip(GameMapChipType.RIGHT_SPLIT).getPrice()
	};

	Object.keys(data).forEach(function(key) {
		if(!data[key]) {
			delete data[key];
		}
		else if(key != 'n') {
			data.s = data.s || 0;
			data.s += data[key];
		}
	});

	return data.s ? data : null;
};

GameMapRow.prototype.setData = function(data) {

	this.addPriceChip(GameMapChipType.CENTER, (data.c || 0));
	this.addPriceChip(GameMapChipType.RIGHT_SPLIT, (data.rs || 0));
};

module.exports = GameMapRow;
