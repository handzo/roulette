/**
 * Created by iShimon on 06.11.16.
 */

function PlayerEvent() {}

PlayerEvent.CREDIT_CHANGE = 'credit_change';
PlayerEvent.BET_CHANGE = 'bet_change';
PlayerEvent.WIN_CHANGE = 'win_change';

module.exports = {
	PlayerEvent : PlayerEvent
};