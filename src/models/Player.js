/**
 * Created by iShimon on 06.11.16.
 */

var EventEmitter = require('eventemitter3');
var PlayerEvent = require('./ModelEvent.js').PlayerEvent;

function Player(name, credit) {

	Player._super.call(this);

	this._name = name;
	this._credit = credit || 0;
	this._bet = 0;
	this._win = 0;
}

utils.inherits(Player, EventEmitter);

Object.defineProperties(Player.prototype, {

	credit: {
		get: function () {
			return this._credit;
		},
		set: function (value) {

			this._credit = value;
			this.emit(PlayerEvent.CREDIT_CHANGE, {
				credit : value
			});
		}
	},

	bet: {
		get: function () {
			return this._bet;
		},
		set: function (value) {

			this._bet = value;
			this.emit(PlayerEvent.BET_CHANGE, {
				bet : value
			});
		}
	},

	win: {
		get: function () {
			return this._win;
		},
		set: function (value) {

			this._win = value;
			this.emit(PlayerEvent.WIN_CHANGE, {
				win : value
			});
		}
	},

	name: {
		get: function () {
			return this._name;
		}
	}
});

module.exports = Player;
