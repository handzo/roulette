module.exports = function (divID, aspectRatio) {

	//function ScreenConfigurator() {
	var div = document.getElementById(divID),
		canvas = document.createElement('canvas');

	div.style.cssText += 'display: flex; justify-content: center; align-items: center;';
	div.appendChild(canvas);

	(window.onresize = function() {
		if (div.offsetWidth > div.offsetHeight / aspectRatio) {
			canvas.style.width = div.offsetHeight / aspectRatio + 'px';
			canvas.style.height = '100%';
		} else {
			canvas.style.width = '100%';
			canvas.style.height = div.offsetWidth * aspectRatio + 'px';
		}
	})();

	return canvas;
	//}

	//return ScreenConfigurator;
};