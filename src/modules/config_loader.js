/**
 * Created by iShimon on 28.09.16.
 */

module.exports = function (app) {
	log('Config loader');

	app.config = {};

	try {
		var context = require.context('json!./../../config', false, /\.json$/);
		context.keys().forEach(function(path) {
			var property_name = path.match(/.\/(.*).json/)[1];
			app.config[property_name] = context(path);
		});
	}
	catch(e) {
		log_error('ConfigLoaderError', 'Directory config not found');
	}
};