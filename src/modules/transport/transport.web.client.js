/**
 * Created by iShimon on 19.06.16.
 */

module.exports = function (options) {

	require('script!./sockjs.min.js');
	var EventEmitter = require('./../event_emitter.js');

	var empty_callback = function () {};

	function TransportWebClient(options) {
		TransportWebClient._super.apply(this, []);

		var self = this;

		var client = {};
		this.closed = true;
		this.reconnect = false;
		this.connecting = false;
		this.reconnect_time = options.reconnect_time || 1000;

		this.counter_key = 1;
		this.requests = {};
		this.request_timeout = options.request_timeout || 5000;

		this.check_ping_monitoring = null;
		this.check_ping_monitoring_curr_tick = 0;
		this.check_ping_monitoring_interval =  (options.check_ping_monitoring_interval || 10) * 1000;
		this.check_ping_monitoring_count_tick = options.check_ping_monitoring_count_tick || 5;
		this.ping = 0;

		if(!options.prefix) {
			log_error('TransportWebClient Options', 'Not found prefix!');
			return;
		}

		if(!options.host || !options.port) {
			log_error('TransportWebClient Options', 'Not found host or port!');
			return;
		}

		if(!options.protocol) {
			log_error('TransportWebClient Options', 'Not found protocol!');
			return;
		}

		this.take_message = function(data) {
			var result = data.toString().split("}{");
			if(result.length > 1) {
				for (var i = 0; i < result.length; i++) {
					var message = result[i];
					if(i != 0) {
						message = "{" + message;
					}
					if(i != result.length - 1) {
						message = message + "}";
					}
					parse_message(message);
				}
			} else {
				parse_message(data.toString());
			}
		};

		var parse_message = function(message) {
			try {
				var json = JSON.parse(message);
				if (!json.package) {
					next_message(message);
				}
				else {
					next_message(json.package);
				}
			}
			catch (exeption) {
				log_error('socket.raw.on("data")', message);
			}
		};

		var next_message = function(message) {
			try {
				var json = JSON.parse(message);
				var data = json.data || {};

				if (!json.event)
					return;
			}
			catch (exeption) {
				log_error('socket.raw.on("data")', message);
				return;
			}

			if(json.event != 'check_ping_monitoring') {
				log_debug('[Transport web client]', 'Take message:', JSON.stringify(json));
			}

			if (json.key) {
				if(self.requests[json.key]) {
					clearTimeout(self.requests[json.key].timeout);
					self.requests[json.key].callback(null, json.data);
					delete self.requests[json.key];
				}
			}
			else {


				self.emit(json.event, data, empty_callback);
				self.emit('message', json, empty_callback);
			}
		};

		this.request = function(event, data, callback, target, not_log) {

			if(typeof data === 'function') {
				callback = data;
				data = {};
			}

			if(self.closed) {
				callback({code: 1000, message : 'Socket closed', result : {closed: true}});
			}
			else {
				var timeout = setTimeout(function () {
					delete self.requests[timeout];
					callback({code: 1001, message : 'Request timeout', result : {timeout: true}});

				}, this.request_timeout);

				this.send(event, data, target, timeout, not_log);
				//timeout.key = 'timeout-' + this.counter_key;

				this.requests[timeout] = {
					callback: callback,
					timeout: timeout
				};
				this.counter_key++;
			}
		};

		this.send_raw = function(message, not_log) {

			if(typeof message !== 'string') {
				message = JSON.stringify(message);
			}

			if(!self.closed) {
				this.send_data(message);

				if(!not_log) {
					log_debug('[Transport client]', 'Send message:', message);
				}
			}
			else {
				log_debug('[Transport client]', 'Not send message. Socket close');
			}
		};

		this.send = function(event, data, target, key, not_log) {

			this.send_raw(JSON.stringify({
				event : event,
				data : data || {},
				target : target,
				key : key
			}), not_log);
		};

		this.check_ping_monitoring_start = function() {

			self.check_ping_monitoring = setInterval(function() {

				self.request('check_ping_monitoring', {
						time : time_now(),
						ping : self.ping
					},
					function(error, data) {
						if(!error) {
							self.ping = time_now() - data.result.time;
							self.check_ping_monitoring_curr_tick--;
							log_debug('Check ping monitoring ' + 'Tick: '+ self.check_ping_monitoring_curr_tick, ' Ping: ' + self.ping);
						}
					}, undefined, true);

				self.check_ping_monitoring_curr_tick++;

				if(self.check_ping_monitoring_curr_tick > self.check_ping_monitoring_count_tick) {
					self.close();
				}

			}, this.check_ping_monitoring_interval)
		};

		this.check_ping_monitoring_stop = function() {

			if(self.check_ping_monitoring) {
				clearInterval(self.check_ping_monitoring);
				self.check_ping_monitoring_curr_tick = 0;
				self.check_ping_monitoring = null;
			}
		};

		this.connect = function() {
			if(self.closed && !self.connecting) {
				self.connecting = true;
				var address = options.protocol + '://' + options.host + ':' + options.port + '/' + options.prefix;
				log('[Transport client]', address);
				client = new SockJS(address);

				client.onopen = function () {
					self.closed = false;

					if(options.is_check_ping_monitoring) {
						log('[Transport client]', 'is_check_ping_monitoring');
						self.check_ping_monitoring_start();
					}

					log('[Transport client]', 'Transport client WS Connect!');
					self.emit('connection', {}, empty_callback, {});
				};

				client.onmessage = function(event) {
					self.take_message(event.data);
				};

				self.close = function() {

					if(!self.closed) {
						client.close();
						self.closed = true;
						self.encrypt_key = null;
						self.check_ping_monitoring_stop();

						log('[Transport client]', 'Transport client WS closed');
						self.emit('close', {}, empty_callback, {});
					}

					self.connecting = false;

					if(!self.reconnect && options.is_reconnect) {
						self.reconnect = true;
						setTimeout(function () {

							log('[Transport client]', 'Transport client WS reconnect', self.reconnect_time, 'mls');
							self.reconnect = false;
							self.connect();
						}, self.reconnect_time);
					}
				};
			}

			client.onclose = self.close;
		};

		this.send_data = function(message) {
			if(client.send) {
				client.send(message);
			}
		};

		if(options.is_reconnect) {
			this.connect();
		}
	}

	inherits(TransportWebClient, EventEmitter);

	return TransportWebClient;
};