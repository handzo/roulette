/**
 * Created by iShimon on 28.09.16.
 */

module.exports = function () {

	global.get_each = function (list, callback, callback_end) {
		// позволяет итерировать по массиву,
		// асинхронно обрабатывая каждый элемент
		// когда элементы заканчиваются, то callback вызывается без параметра

		var finish = function () {
			if (callback_end) {
				callback_end();
			}
			else {
				callback(null);
			}
		};

		if (!list || !list.length) {
			finish();
			return;
		}

		var index = 0;
		var length = list.length;

		var next = function () {
			callback(list[index], function () {
				index++;

				if (index < length) {
					next();
				}
				else {
					finish();
				}
			}, index);
		};

		next();
	};

	global.each_interval = function(start, end, callback, callback_end) {

		var finish = function () {
			if (callback_end) {
				callback_end();
			}
			else {
				callback(null);
			}
		};

		var index = start;

		var next = function() {
			if(index < end) {
				callback(index, function() {
					index++;
					next();
				});
			}
			else {
				finish();
			}
		};

		next();
	};

	global.each_prop = function (obj, callback, callback2) {

		var finish = function () {
			if (callback2)
				callback2();
			else
				callback(null);
		};

		if (!obj) {
			finish();
			return;
		}

		get_each(Object.keys(obj), function (prop, next_prop) {
			if (!prop) {
				finish();
				return;
			}
			callback(prop, obj[prop], next_prop);
		});
	};

	global.SEQ = function (func_list)
	{
		if (typeof func_list === 'function')
		{
			func_list = Array.prototype.slice.call(arguments, 0);
		}

		var next = function ()
		{
			var func = func_list.shift();

			if (!func)
				return;

			var args = Array.prototype.slice.call(arguments, 0);

			if (func_list.length > 0)
				args.unshift(next);

			func.apply(this, args)
		};

		next();
	};

	global.PRL = function (func_list)
	{
		if (typeof func_list === 'function')
			func_list = Array.prototype.slice.call(arguments, 0);

		var result = {};
		var func_count = func_list.length;
		var counter = func_count - 1;

		for (var i = 0; i < func_count - 1; i++)
		{
			func_list[i](function (res)
			{
				counter--;

				if (res)
					for (var prop in res)
						result[prop] = res[prop];

				if (counter === 0)
					func_list[func_count - 1](result);
			});
		}
	};

	global.inherits = function(clazz, super_clazz) {
		clazz.prototype = Object.create(super_clazz.prototype);
		clazz.prototype.constructor = clazz;
		clazz._super = super_clazz;
	};

	global.rand = function(max, min) {

		max = max || 1000;
		min = min || 0;

		return parseInt(Math.random() * (max - min) + min);
	};

	global.add_key_to_object = function(obj1, obj2) {
		for(var key in obj2) {
			obj1[key] = obj2[key];
		}
		return obj1;
	};

	global.merge_objects_keys = function() {
		var result = {};
		for(var i = 0; i < arguments.length; i++) {
			for(var key in arguments[i]) {
				result[key] = arguments[i][key];
			}
		}
		return result;
	};

	global.override_method = function () {
		throw 'OverrideError';
	};

	Array.prototype.last = function () {
		return this[this.length - 1];
	};

	Array.prototype.first = function () {
		return this[0];
	};

	var decToHex = function(d) {
		if(d > 15) {
			return d.toString(16)
		} else {
			return "0"+d.toString(16)
		}
	};

	global.rgbToHex = function(r,g,b) {
		return "#" + decToHex(r) + decToHex(g) + decToHex(b);
	};

	global.degToRad = function(angle) {
		return angle * Math.PI / 180;
	};
};
