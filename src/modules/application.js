/**
 * Created by iShimon on 27.09.16.
 */

module.exports = function (options) {

	var EventEmitter = require('./event_emitter.js');

	var app = new EventEmitter();
	app.root_dir = options.app_root_dir;
	app.name = options.name

	return app;
};