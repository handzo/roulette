/**
 * Created by iShimon on 27.09.16.
 */

module.exports = function () {

	global.log = function() {
		arguments = Array.prototype.slice.call(arguments);
		arguments.unshift('[ Curr Date ]');
		return console.log.apply(console, arguments);
	};

	global.log_debug = function() {
		arguments = Array.prototype.slice.call(arguments);
		arguments.unshift('Debug:');
		global.log.apply(global, arguments);
	};

	global.log_error = function (error, message) {
		if (typeof(message) == 'object') {
			message = JSON.stringify(message);
		}
		log('\nError: ' + error +'\n' + 'Message: ' + message + '\n');
	};
};