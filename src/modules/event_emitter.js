/**
 * Created by iShimon on 27.07.16.
 */

function EventEmitter() {
	this.callbacks = {};
}

EventEmitter.prototype.on = function(name, callback) {
	var funcs = this.callbacks[name] || [];
	funcs.push(callback);
	this.callbacks[name] = funcs;
};

EventEmitter.prototype.remove_event_listener = function(name, callback) {

	var funcs = this.callbacks[name] || [];
	var index = funcs.indexOf(callback);

	if(index != -1) {
		funcs.splice(index, 1);
		if(!funcs.length) {
			delete this.callbacks[name];
		}
	}
};

EventEmitter.prototype.emit = function() {
	arguments = Array.prototype.slice.call(arguments);
	var name = arguments.shift();
	var funcs = this.callbacks[name] || [];

	for(var i = 0; i < funcs.length; i++) {
		funcs[i].apply(this, arguments);
	}
};

module.exports = EventEmitter;
