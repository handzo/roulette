/**
 * Created by iShimon on 25.10.16.
 */

var GameMap = require('./../game_map/GameMap.js');
var EllipseGameMap = require('./../game_map/EllipseGameMap.js');
var GameMapEvent = require('./../game_map/GameMapEvent.js').GameMapEvent;
var TableChipBox = require('./TableChipBox.js');
var TableChipBoxEvent = require('./GameTableEvent').TableChipBoxEvent;
var PlayerEvent = require('../models/ModelEvent.js').PlayerEvent;
var EllipseGameMapChangedEvent = require('./../game_map/GameMapEvent.js').EllipseGameMapChangedEvent;
var GameMapChangedEvent = require('./../game_map/GameMapEvent.js').GameMapChangedEvent;
var GameWinHistory = require('./GameWinHistory.js');

function GameTable(limit, playerModel) {

	GameTable._super.call(this);

	this.playerModel = playerModel;
	this.gameMapView = true;

	var self = this;

	this.colors = ['Green', 'Red', 'Blue', 'Violet'];
	this.currentColor = 0;

	this.background = helper.createSprite('Roulette', 'Background' + this.colors[this.currentColor], this);

	this.style = {
		fontFamily : 'Arial',
		fontSize : '25px',
		fill : '#FFFFFF'
	};

	this.textPlayer = helper.createText('Приветствуем вас: ' + this.playerModel.name + '!', this.style, this.background, {x: 960, y: 16}, {x: 0.5, y: 0.5});
	this.textCredit = helper.createText('БАЛАНС: ' + this.playerModel.credit, this.style, this.background, {x: 700, y: 82}, {x: 0.5, y: 0.5});

	this.playerModel.on(PlayerEvent.CREDIT_CHANGE, function(event) {
		this.textCredit.text = 'БАЛАНС: ' + event.credit;
	}, this);

	this.textBet = helper.createText('СТАВКА: 0', this.style, this.background, {x: 960, y: 82}, {x: 0.5, y: 0.5});

	this.playerModel.on(PlayerEvent.BET_CHANGE, function(event) {
		this.textBet.text = 'СТАВКА: ' + event.bet;
	}, this);

	this.textWin = helper.createText('ВЫИГРЫШ: 0' , this.style, this.background, {x: 1220, y: 82}, {x: 0.5, y: 0.5});

	this.playerModel.on(PlayerEvent.WIN_CHANGE, function(event) {
		this.textWin.text = 'ВЫИГРЫШ: '  + event.win;
	}, this);

	this.styleNumber = {
		fontFamily : 'Arial',
		fontSize : '30px',
		fill : '#FFFFFF',
	};

	this.textNumber = helper.createText('ДЕЛАЙТЕ ВАШИ СТАВКИ' , this.styleNumber, this.background, {x: 960, y: 148}, {x: 0.5, y: 0.5});

	this.chip_box = new TableChipBox([-1, 10, 25, 50, 100, 500], 10);
	this.chip_box.x = 655 + 45;
	this.chip_box.y = 368 + 60;
	this.chip_box.scale.x = 0.75;
	this.chip_box.scale.y = 0.75;
	this.chip_box.switchChipAnim(10);
	this.chip_box.on(TableChipBoxEvent.SWITCH_CHIP, function(event) {
		this.gameMap.setSelectChip(event.price);
		this.ellipseGameMap.setSelectChip(event.price);
	}, this);
	this.background.addChild(this.chip_box);

	this.clearButton = helper.createButton('Roulette', {up: 'ButtonCancelUp', down: 'ButtonCancelDown'}, this.background, {x: 1180, y: 190});
	this.clearButton.on(core.ButtonEvent.BUTTON_DOWN, function(event) {
		this.gameMap.clearAll();
		this.ellipseGameMap.clearAll();
	}, this);

	this.complectButton = helper.createButton('Roulette', {up: 'ButtonComplectUp', down: 'ButtonComplectDown'}, this.background, {x: 785, y: 190});
	this.complectButton.on(core.ButtonEvent.BUTTON_DOWN, function(event) {
		this.gameMap.complect();
	}, this);

	this.doubleButton = helper.createButton('Roulette', {up: 'ButtonDoubleUp', down: 'ButtonDoubleDown'}, this.background, {x: 990, y: 190});
	this.doubleButton.on(core.ButtonEvent.BUTTON_DOWN, function(event) {
		// log(self.gameMap.getData());
		// log(self.ellipseGameMap.getData());
		this.gameHistory.push(parseInt(Math.random() * 37));

		// self.test = self.getData();
	}, this);

	this.repeatButton = helper.createButton('Roulette', {up: 'ButtonRepeatUp', down: 'ButtonRepeatDown'}, this.background, {x: 605, y: 190});
	this.repeatButton.on(core.ButtonEvent.BUTTON_DOWN, function(event) {
		//	self.place_your_bets();
		// this.setData(this.test);
	}, this);

	this.switchColorButton = helper.createButton('Roulette', {up: 'ButtonColorUp', down: 'ButtonColorDown'}, this.background, {x: 1780, y: 649});
	this.switchColorButton.on(core.ButtonEvent.BUTTON_DOWN, this.nextBackgroundColor, this);

	this.soundButton = helper.createCheckBox('Roulette', {on: 'ButtonSoundOn', off: 'ButtonSoundOff'}, this.background, {x: 1780, y: 794});
	this.soundButton.state = true;
	this.soundButton.on(core.CheckBoxEvent.SWITCH_STATE, function(event) {
		//self.nextBackgroundColor();
	});

	this.settingsButton = helper.createButton('Roulette', {up: 'ButtonSettingsUp', down: 'ButtonSettingsDown'}, this.background, {x: 1780, y: 940});
	this.settingsButton.on(core.ButtonEvent.BUTTON_DOWN, function(event) {
		//self.nextBackgroundColor();
	});

	this.switchGameMapButton = helper.createButton('Roulette', {up: 'ButtonSwitchUp', down: 'ButtonSwitchDown'}, this.background, {x: 1780, y: 504});
	this.switchGameMapButton.on(core.ButtonEvent.BUTTON_DOWN, function(event) {
		this.background.removeChild(this.gameMapView ? this.gameMap : this.ellipseGameMap);
		this.background.addChild(this.gameMapView ? this.ellipseGameMap : this.gameMap);
		this.gameMapView = !this.gameMapView;
	}, this);

	this.gameMap = new GameMap(108, 108, 3, 20, limit, playerModel);
	this.gameMap.x = 320 ;
	this.gameMap.y = 542;
	this.background.addChild(this.gameMap);

	this.ellipseGameMap = new EllipseGameMap(limit, playerModel);
	this.ellipseGameMap.x = 202;
	this.ellipseGameMap.y = 622;

	this.gameMap.on(GameMapChangedEvent.GAME_MAP_CHANGED, function(event) {
		this.ellipseGameMap.eventGameMapChanged(event);
	}, this);
	this.ellipseGameMap.on(EllipseGameMapChangedEvent.ELLIPSE_GAME_MAP_CHANGED, function(event) {
		this.gameMap.eventEllipseGameMapChanged(event);
	}, this);

	this.is_place_your_bets = false;
	this.is_bets_is_over = false;
	this.is_win_number = true;

	//app.transport.on('web_terminal_roulette', function(data) {
	//	if(data.status == 'place_your_bets') {
	//		if(!self.is_place_your_bets && !self.is_bets_is_over && self.is_win_number) {
	//			self.is_place_your_bets = true;
	//			self.is_win_number = false;
	//			self.place_your_bets();
	//		}
	//	}
	//	else if(data.status == 'bets_is_over') {
	//		if(self.is_place_your_bets && !self.is_bets_is_over) {
	//			self.is_bets_is_over = true;
	//			self.is_place_your_bets = false;
	//			self.bets_is_over();
	//		}
	//	}
	//	else if(data.status == 'win_number') {
	//
	//		if(!self.is_win_number && self.is_bets_is_over && !self.is_place_your_bets) {
	//
	//			self.is_bets_is_over = false;
	//			self.is_win_number = true;
	//			self.winNumber(data.number);
	//		}
	//	}
	//})

	this.gameHistory = new GameWinHistory(20, this.gameMap.colors);
	this.gameHistory.x = 37;
	this.gameHistory.y = 520;
	this.gameHistory.setHistory([3,1,21,33,5,16,24,0,31,2,13,30]);
	this.addChild(this.gameHistory);
}

utils.inherits(GameTable, PIXI.Graphics);

GameTable.prototype.nextBackgroundColor = function() {
	this.currentColor++;
	this.background.setResAnim(helper.createTexture('Roulette', 'Background'+ this.colors[this.currentColor % this.colors.length]));
};

GameTable.prototype.getData = function() {

	return {
		gameMap : this.gameMap.getData(),
		ellipseMap : this.ellipseGameMap.getData(),
		sum : this.playerModel.bet
	};
};

GameTable.prototype.setData = function(data) {

	this.gameMap.clearAll();
	this.gameMap.setData(data.gameMap);

	this.ellipseGameMap.clearAll();
	this.ellipseGameMap.setData(data.ellipseMap);
};

GameTable.prototype.place_your_bets = function() {
	this.textNumber.text = 'ДЕЛАЙТЕ ВАШИ СТАВКИ';
	this.ellipseGameMap.setIsBet(true);
	this.gameMap.setIsBet(true);
	this.ellipseGameMap.clear();
	this.gameMap.clear();
};

GameTable.prototype.bets_is_over = function() {
	this.textNumber.text = 'СТАВОК БОЛЬШЕ НЕТ';
	this.gameMap.setIsBet(false);
	this.ellipseGameMap.setisBet(false);
};

GameTable.prototype.winNumber = function(number) {
	this.textNumber.text = 'ВЫПАВШЕЕ ЧИСЛО: ' + number;
	this.gameHistory.push(number);
	this.ellipseGameMap.winNumber(number);
	this.gameMap.winNumber(number);
};

module.exports = GameTable;
