var GameMapCellColor = require('./../game_map/GameMapEnum.js').GameMapCellColor;

function GameWinHistory(size, colors) {

    GameWinHistory._super.call(this);
    this.size = size;
    this.colors = colors;
    this.countRow = colors.length;
    this.countCol = colors[0].length;
    this.allowedToPush = true;

    this.history = [];

    this.black   = {fontFamily : 'Arial', fill : '#FFFFFF', fontSize : 30};
    this.red     = {fontFamily : 'Arial', fill : '#FF0000', fontSize : 30};
    this.green   = {fontFamily : 'Arial', fill : '#00FF00', fontSize : 30};

}

utils.inherits(GameWinHistory, PIXI.Graphics);

GameWinHistory.prototype.setHistory = function(history) {

    this.history = [];

    for (var i = 0; i < history.length; i++) {
        var record = this.createRecord(history[i]);
        record.y = (history.length - 1 - i) * 26;
        this.history.push(record);
        this.addChild(record);
    }
};

GameWinHistory.prototype.createRecord = function(number) {
    var record = new PIXI.Text(number);

    if (number == 0) {
        record.text = '0';
        record.style = this.green;
        record.anchor.x = 0.5;
        record.x = 48;
    }
    else {
        var x = parseInt((number - 1) / this.countRow);
        var y = this.countRow - 1 - parseInt((number - 1) % this.countRow);

        if (this.colors[y][x] === GameMapCellColor.RED) {
            record.style = this.red;
            record.x = 0;
        }
        else if (this.colors[y][x] === GameMapCellColor.BLACK) {
            record.style = this.black;
            record.anchor.x = 1;
            record.x = 96;
        }
    }

    return record;
};



GameWinHistory.prototype.push = function(number) {

    if (!this.allowedToPush) {
        return;
    }
    this.allowedToPush = false;

    var record = this.createRecord(number);

    if (record) {
        record.y = -26;
        record.alpha = 0;

        this.shift();
        this.history.push(record);
        this.addChild(record);

        var addRecordTween = new PIXI.Tween({alpha : 1, y : 0}, {duration : 500});

        addRecordTween.on(PIXI.TweenEvent.COMPLETE, function() {
            this.allowedToPush = true;
        }, this);

        record.addTween(addRecordTween);
    }
};

GameWinHistory.prototype.shift = function() {

    if (this.history.length) {
        if (this.history.length >= this.size) {
            var dissapearTween = new PIXI.Tween({alpha : 0, y : this.history.y + 26}, {duration : 500});
            dissapearTween.on(PIXI.TweenEvent.COMPLETE, function() {
                this.history[0].detach();
                this.history.shift();
            }, this);
            this.history[0].addTween(dissapearTween);
        }

        for (var i = 0; i < this.history.length; i++) {
            this.history[i].addTween(new PIXI.Tween({y : this.history[i].y + 26}, {duration : 500}));
        }
    }
};

module.exports = GameWinHistory;
