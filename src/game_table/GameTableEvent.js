function TableChipEvent() {}
TableChipEvent.SWITCH = 'switch';

function TableChipBoxEvent() {}
TableChipBoxEvent.SWITCH_CHIP = 'switch_chip';

module.exports = {
	TableChipEvent : TableChipEvent,
	TableChipBoxEvent : TableChipBoxEvent
};
