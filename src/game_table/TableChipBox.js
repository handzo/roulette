/**
 * Created by iShimon on 25.10.16.
 */

var TableChip = require('./TableChip');
var TableChipBoxEvent = require('./GameTableEvent').TableChipBoxEvent;
var TableChipEvent = require('./GameTableEvent').TableChipEvent;

function TableChipBox(chip_prices, step) {

	TableChipBox._super.call(this);

	var self = this;
	this.chips = [];

	for(var i = 0; i < chip_prices.length; i++) {
		var tableChip = new TableChip(i, chip_prices[i]);
		tableChip.position.x = i * (128 + step);
		tableChip.on(TableChipEvent.SWITCH, this.eventChipButtonSwitch, this);
		this.addChild(tableChip);
		this.chips.push(tableChip);
	}
}

utils.inherits(TableChipBox, PIXI.Sprite);

TableChipBox.prototype.eventChipButtonSwitch = function(event) {

	for(var i = 0; i < this.chips.length; i++) {
		this.chips[i].setState(event.number == this.chips[i].getNumber());
	}

	this.emit(TableChipBoxEvent.SWITCH_CHIP, {price : event.price});
};

TableChipBox.prototype.switchChip = function(price) {
	for(var i = 0; i < this.chips.length; i++) {
		this.chips[i].setActive(price == this.chips[i].price);
	}
};

TableChipBox.prototype.switchChipAnim = function(price) {
	for(var i = 0; i < this.chips.length; i++) {
		this.chips[i].setState(price == this.chips[i].price);
	}
};

module.exports = TableChipBox;
