/**
 * Created by iShimon on 25.10.16.
 */

var TableChipEvent = require('./GameTableEvent').TableChipEvent;

function TableChip(number, price) {

	TableChip._super.call(this);

	this.price = price;
	this.number = number;
	this.state = false;

	this.skin = helper.createSprite('Roulette', 'ChipAnimUp_' + price, this, {}, {x: 0.5, y: 0.5});
	this.skin.setAnimFrame(this.skin.texture, 7);

	this.activeArea = helper.drawRect(0xFFFFFF, -64, -64, 128, 128, this);
	this.activeArea.alpha = 0;

	this.hitArea = this.activeArea.getBounds();
	this.interactive = true;

	this.on('mousedown', this.eventClick);
	this.on('touchstart', this.eventClick);
}

utils.inherits(TableChip, PIXI.Graphics);

TableChip.prototype.setState = function(state) {

	if(this.state == state) {
		return;
	}

	this.state = state;
	this.skin.removeTweens();
	if(state) {
		this.upward();
		this.skin.addTween(new PIXI.TweenAnim(helper.createTexture('Roulette', 'ChipAnimUp_' + this.price), {duration : 1200}));
	}
	else {
		this.skin.addTween(new PIXI.TweenAnim(helper.createTexture('Roulette', 'ChipAnimDown_' + this.price), {duration : 300}));
	}
};

TableChip.prototype.getState = function() {
	return this.state;
};

TableChip.prototype.getPrice = function() {
	return this.price;
};

TableChip.prototype.getNumber = function() {
	return this.number;
};

TableChip.prototype.eventClick = function() {

	this.emit(TableChipEvent.SWITCH, {
		price : this.price,
		number : this.number
	});
};

TableChip.prototype.setActive = function(state) {

	this.state = state;
	this.skin.removeTweens();
	if(state) {
		this.upward();
		this.skin.setAnimFrame(helper.createTexture('Roulette', 'ChipAnimUp_' + this.price), 26);
	}
	else {
		this.skin.setAnimFrame(helper.createTexture('Roulette', 'ChipAnimDown_' + this.price), 7);
	}
};

module.exports = TableChip;
