/**
 * Created by iShimon on 02.10.16.
 */

var renderer = new PIXI.WebGLRenderer(app.config.slot_game.screen_width, app.config.slot_game.screen_height);
document.body.appendChild(renderer.view);

app.stage = new PIXI.Container();
app.stage.root_width = app.config.slot_game.screen_width;
app.stage.root_height = app.config.slot_game.screen_height;

app._frames = 0;

var debug = new core.DebugObject();

var GameTable = require('./game_table/GameTable.js');
var Player = require('./models/Player.js');

function app_preinit(callback) {
	callback();
}

function app_init(callback) {

	SEQ(function load_roullete(next) {
		core.ResourceManager.instance().addResource('Roulette');
		core.ResourceManager.instance().loadResource('Roulette', require('json!~Resources/roulette.json'), next, function(data) {
			log(data.progress);
		});
		// ResourceManager.instance().addResourse('Roulette');
		// ResourceManager.instance().loadResource('Roulette','roulette.json', next, function(data) {
		// //	log(data.progress);
	},
	function create_scene() {
		var playerModel = new Player('Player1', 100000);

		var gameTable = new GameTable(15000, playerModel);
		app.stage.addChild(gameTable);

		debug.show();
		callback();
	});
}

function app_update() {

	requestAnimationFrame(app_update);
	app.stage.update();
	renderer.render(app.stage);
}

function app_destroy() {

}

module.exports = {
	app_preinit : app_preinit,
	app_init : app_init,
	app_update : app_update,
	app_destroy : app_destroy
};
