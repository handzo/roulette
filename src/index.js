/**
 * Created by iShimon on 27.09.16.
 */

global.app = require('./../../web-common')({
	appRootDir : __dirname,
	name : 'web-roulette'
});

var main = require('./app.js');

main.app_preinit(function() {
	main.app_init(function() {
		main.app_update();
	});
});
